/*------------------------------
Ouverture vers un agrandissement:
-> donner de l'intérêt à des batisses complexes (et non un gros pavé)
-> ajouter différents types de briques (bords? toits? couleurs?)
-> faire quelque chose de l'excédent de rock/lumber
-> Import/Export des sauvegardes
-> Répéter un bâtiment donne des chances qu'une brique sorte dorée? (plus de sous/monnaie de prestige?)
-> arbre de prestige sous forme de chateau fort (un upgrade une brique)
-> automatiser la répartition des travailleurs
-> ajouter un zoomLevel (est-ce que l'algo de filling() est assez performant pour ça?)

/*------------------------------
1.2 Road to prestige

PRESTIGE
X-> la retraite débloque un nouveau menu prestige, et une nouvelle monnaie qui permet d'acheter les upgrades
X-> visuel prestige: bouton + menu
X-> menu représente un chateau à construire brique par brique
X-> chaque brique est cliquable et à un tooltip (objet BrickPrestige)
X-> bouton "ok" pour redémarrer qui réinitialise le jeu (sauf le prestige)
X-> message de vérification
X-> on ne voit la brique au dessus (avec son tooltip) que si celle d'en dessous est construite

MECANIQUES
X-> chance de créer une brique dorée calculée lors de la construction (qui donne une brique prestige)
X-> golden bricks: color attribute

AUTOMATISATION
-> répartition automatique des travailleurs en fonction du remplissage des stocks
-> achat automatique d'upgrades (différents niveaux d'automatisation)
    -> dumb : achète le moins cher dès que possible
    -> average : dumb + privilégier un upgrade
    -> smart : ordre d'upgrades déterminé

SAUVEGARDE
X-> ajouter le nouveau système de prestige
X-> ajouter un version control qui ajoute le système de prestige si la version est antérieure
X-> système d'import/export de sauvegarde

NETTOYAGE
X-> nettoyer la méthode clic(): mettre un clicEffect booléen pour ne pas tout tester

/*------------------------------
BUGS

X-> si on ouvre option et help ils se superposent
X-> si les workers coutent trop cher, bloquer l'affichage au dela de 1e+10 (oui ça marche tel quel)
-> buildingHandler a un building de retard pour afficher en arrière fond
-> musique se comporte bizarrement sur l'erase

/*------------------------------
Variables globales
*/ 
 
//canvas simple n'est plus utilisé, mais si j'en ai oublié un quelque part c'est bien que ça reste par là
var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
context.imageSmoothingEnabled = false;
canvas.width = 1300;
canvas.height = 700;

//canvas layers
var canvasBackground = document.getElementById('canvasBackground');
var ctxBG = canvasBackground.getContext('2d');
var canvasDrawingZone = document.getElementById('canvasDrawingZone');
var ctxDZ = canvasDrawingZone.getContext('2d');
var canvasFront = document.getElementById('canvasFront');
var ctxFront = canvasFront.getContext('2d');
var canvasBrick = document.getElementById('canvasBrick');
var ctxBrick = canvasBrick.getContext('2d');
var canvasScaffold = document.getElementById('canvasScaffold');
var ctxScaffold = canvasScaffold.getContext('2d');
var canvasWorker = document.getElementById('canvasWorker');
var ctxWorker = canvasWorker.getContext('2d');
var canvasPrestige = document.getElementById('canvasPrestige');
var ctxPrest = canvasPrestige.getContext('2d');
var canvasPrestigeHover = document.getElementById('canvasPrestigeHover');
var ctxPHover = canvasPrestigeHover.getContext('2d');

//mouse
var canvasPosition = canvas.getBoundingClientRect();
const mouse = {
    x: undefined,
    y: undefined,
};
//time
var refresh = 10;
var lastTime = 0;
var timer;
//img
var workersImg = new Array();
var mine, tree1, tree2, scaffold, forest, ground, ground2;
var imgUp = {dollar: new Image(), speed: new Image(), height: new Image(), surface: new Image(), width: new Image()};
var imgMenu = {option: new Image(), musicOn: new Image(), musicOff: new Image(), help: new Image(), save: new Image()};
var imgBK = {cloud1: new Image(), cloud2: new Image(), hill1: new Image(), hill2: new Image(), mountain: new Image(), sky: new Image(), tree1: new Image(), tree2: new Image()};
var imgLoading = {total: 4+7+8+10, current: 0};
//import/export
var importExport = document.getElementById("importExport");
var infoImportExport = document.getElementById("infoImportExport");
var contentImportExport = document.getElementById("contentImportExport");

/*------------------------------
Initialisation
*/

function initializeImg() {
    imgLoading.current = 0;

    workersImg = new Array();
    let w = ["Adam", "Alex", "Amelia", "Bob"];
    for(let i=0; i<4; i++) {
        workersImg[i] = new Image();
        workersImg[i].onload = function() { oneMoreImgLoad() };
        workersImg[i].src = "img/" + w[i] + "_run_16x16.png";
    }
    mine = new Image();
    mine.onload = function() { oneMoreImgLoad() };
    mine.src = "img/mine.png";
    tree1 = new Image();
    tree1.onload = function() { oneMoreImgLoad() };
    tree1.src = "img/tree20.png";
    tree2 = new Image();
    tree2.onload = function() { oneMoreImgLoad() };
    tree2.src = "img/tree21.png";
    scaffold = new Image();
    scaffold.onload = function() { oneMoreImgLoad() };
    scaffold.src = "img/scaffold.png";
    forest = new Image();
    forest.onload = function() { oneMoreImgLoad() };
    forest.src = "img/forest.png";
    ground = new Image();
    ground.src = "img/ground.png";
    ground.onload = function() { oneMoreImgLoad() };
    ground2 = new Image();
    ground2.onload = function() { oneMoreImgLoad() };
    ground2.src = "img/ground2.png";
    imgMenu.option.onload = function() { oneMoreImgLoad() };
    imgMenu.option.src = "img/option.png";
    imgMenu.musicOn.onload = function() { oneMoreImgLoad() };
    imgMenu.musicOn.src = "img/musicOn.png";
    imgMenu.musicOff.onload = function() { oneMoreImgLoad() };
    imgMenu.musicOff.src = "img/musicOff.png";
    imgMenu.help.onload = function() { oneMoreImgLoad() };
    imgMenu.help.src = "img/help.png";
    imgMenu.save.onload = function() { oneMoreImgLoad() };
    imgMenu.save.src = "img/save.png";
    imgUp.dollar.onload = function() { oneMoreImgLoad() };
    imgUp.dollar.src = "img/dollar.png";
    imgUp.speed.onload = function() { oneMoreImgLoad() };
    imgUp.speed.src = "img/speed.png";
    imgUp.surface.onload = function() { oneMoreImgLoad() };
    imgUp.surface.src = "img/surface.png";
    imgUp.width.onload = function() { oneMoreImgLoad() };
    imgUp.width.src = "img/width.png";
    imgUp.height.onload = function() { oneMoreImgLoad() };
    imgUp.height.src = "img/height.png";
    Object.keys(imgBK).forEach(key => {
        imgBK[key].onload = function() { oneMoreImgLoad() };
        imgBK[key].src = "img/" + key + ".png";
    });
}

/*------------------------------
Classes
*/

/**
 * @typedef {Object} Brick
 * @property {number} x point top left of the brick
 * @property {number} y 
 * @property {number} sizeX width
 * @property {number} sizeY height
 * @property {bool} built becomes true when built
 * @property {string} flag undefined, becomes "taken" when a worker works on it
 */
class Brick {
    /**
     * @constructor
     * @param {number} x left
     * @param {number} y top
     * @param {number} sizeX width
     * @param {number} sizeY height
     */
    constructor(x, y, sizeX, sizeY) {
        this.x = x;
        this.y = y;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.built = false; 
        this.flag = undefined;
        this.golden = false;
    }
    becomeGolden() {
        this.golden = true;
    }
    draw() {
        if(this.built || gameHandler.state == "validateConstruction") {
            ctxBrick.lineWidth = 2;
            if(this.built && !this.golden) {
                ctxBrick.fillStyle = 'rgba(0, 0, 0, 1)';
                ctxBrick.fillRect(this.x-1, this.y-1, this.sizeX+2, this.sizeY+2);
                ctxBrick.fillStyle = "rgba(228, 233, 237, 1)";
                ctxBrick.fillRect(this.x+1, this.y+1, this.sizeX-2, this.sizeY-2);
                ctxBrick.fillStyle = 'rgba(109, 113, 122, 1)';
                ctxBrick.fillRect(this.x+2, this.y+2, this.sizeX-3, this.sizeY-3);
            } else if(this.built && this.golden) {
                ctxBrick.fillStyle = 'rgba(186, 143, 35, 1)';
                ctxBrick.fillRect(this.x-1, this.y-1, this.sizeX+2, this.sizeY+2);
                ctxBrick.fillStyle = "rgba(228, 233, 237, 1)";
                ctxBrick.fillRect(this.x+1, this.y+1, this.sizeX-2, this.sizeY-2);
                ctxBrick.fillStyle = 'rgba(252, 195, 53, 1)';
                ctxBrick.fillRect(this.x+2, this.y+2, this.sizeX-3, this.sizeY-3);
            } else {
                ctxBrick.fillStyle = "rgba(0, 0, 255, 0.1)";
                ctxBrick.fillRect(this.x+1, this.y+1, this.sizeX-2, this.sizeY-2);
                ctxBrick.strokeStyle = 'rgba(0, 0, 255, 0.6)';
                ctxBrick.fillStyle = 'rgba(0, 0, 255, 0.1)';
                ctxBrick.fillRect(this.x+2, this.y+2, this.sizeX-3, this.sizeY-3);
                ctxBrick.strokeRect(this.x, this.y, this.sizeX, this.sizeY);
            }
        }
    }
}

class Scaffold {
    constructor(x, y, sizeX, sizeY) {
        this.x = x;
        this.y = y;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.built = false;
        this.flag = undefined;
        let w = Math.floor(this.sizeX/4);
        let x1 = Math.round(this.sizeX/2 - w/2);
        this.ladder = {x: x1+this.x, w: w};
    }
    draw() {
        if(this.built) {
            ctxScaffold.drawImage(scaffold, this.x, this.y, this.sizeX, this.sizeY);
        }
    }
}

class LambdaWorker {
    constructor(job, cooldown, speed, width, height, offsetX = 0) {
        this.job = job;
        this.img = workersImg[Math.floor(Math.random() * workersImg.length)];
        this.currentAnim = 0;
        this.maxAnim = 6;
        this.direction = "left";
        this.width = Math.floor(width/UIHandler.zoomLevel);
        this.height = Math.floor(height/UIHandler.zoomLevel);
        this.speed = speed;
        let customY = UIHandler.drawSurface.y + UIHandler.drawSurface.h - this.height;
        this.y = customY;
        this.goal = {
            mine: {x: UIHandler.mine.x + Math.round(UIHandler.mine.w/2), y: customY, name: "mine"},
            stockRock: {x: UIHandler.stockRock.x + Math.round(UIHandler.stockRock.w/2), y: customY, name: "stockRock"},
            forest: {x: UIHandler.forest.x + Math.round(UIHandler.forest.w/2), y: customY, name: "forest"},
            stockLumber: {x: UIHandler.stockLumber.x + Math.round(UIHandler.stockLumber.w/2), y: customY, name: "stockLumber"},
            block: undefined,
        }
        this.nextFloor = undefined;
        this.maxCooldown = cooldown;
        this.currentCooldown = 0;
        switch(job) {
            case "miner":
                this.x = offsetX+ (this.goal.mine.x + this.goal.stockRock.x)/2;
                this.curGoal = this.goal.mine;
                break;
            case "lumberjack":
                this.x = offsetX+ (this.goal.stockLumber.x + this.goal.forest.x)/2;
                this.curGoal = this.goal.forest;
                break;
            case "builder":
            default:
                this.x = offsetX+ (this.goal.stockRock.x + this.goal.stockLumber.x)/2;
                this.curGoal = undefined;
                break;
        }
        this.transporting = undefined;
    }
    turn(dir) {
        this.direction = dir;
    }
    move() {
        this.currentAnim++;
        if(this.currentAnim >= this.maxAnim*10) 
            this.currentAnim = 0;
        switch(this.direction) {
            case "right":
                this.x += this.speed;
                break;
            case "top":
                this.y -= this.speed;
                break;
            case "left":
                this.x -= this.speed;
                break;
            case "bottom":
                this.y += this.speed;
                break;
        }
    }
    draw() {
        if(this.currentCooldown == 0) {
            let sx = 16*6;
            switch(this.direction) {
                case "right":
                    sx *= 0;
                    break;
                case "top":
                    sx *= 1;
                    break;
                case "left":
                    sx *= 2;
                    break;
                case "bottom":
                    sx *= 3;
                    break;
            }
            sx += 16*Math.floor(this.currentAnim/10);
            //porte un caillou
            if(this.transporting == "rock") {
                ctxWorker.lineWidth = 2;
                ctxWorker.strokeStyle = 'rgba(0, 0, 0, 1)';
                ctxWorker.fillStyle = 'rgba(109, 113, 122, 1)';
                let x = this.x + this.width/2;
                let y = this.y + this.height/4;
                let sizeX = this.width/2;
                let sizeY = this.height/2;
                switch(this.direction) {
                    case "left":
                        //x += this.width;
                        break;
                    case "right":
                        x -= sizeX;
                        break;
                    case "top":
                        x -= sizeX/2;
                        break;
                }
                if(this.direction == "top") {
                    ctxWorker.drawImage(this.img, sx*4, 0, 16*4, 32*4, this.x, this.y, this.width, this.height);
                }
                ctxWorker.strokeRect(x, y, sizeX, sizeY);
                ctxWorker.fillRect(x+1, y+1, sizeX-2, sizeY-2);
            }
            //porte du bois
            if(this.transporting == "lumber") {
                ctxWorker.lineWidth = 2;
                ctxWorker.strokeStyle = 'rgba(0, 0, 0, 1)';
                ctxWorker.fillStyle = 'rgba(164, 116, 73, 1)';
                let x = this.x + this.width/2;
                let y = this.y + this.height/4;
                let sizeX = this.width/2;
                let sizeY = this.height/2;
                if(this.direction == "right")
                    x -= sizeX;
                if(this.direction == "top") {
                    x -= sizeX/2;
                    ctxWorker.drawImage(this.img, sx*4, 0, 16*4, 32*4, this.x, this.y, this.width, this.height);
                }
                ctxWorker.strokeRect(x, y, sizeX, sizeY);
                ctxWorker.fillRect(x+1, y+1, sizeX-2, sizeY-2);
            }
            if(this.transporting === undefined || this.direction != "top")
                ctxWorker.drawImage(this.img, sx*4, 0, 16*4, 32*4, this.x, this.y, this.width, this.height);
        }
    }
}

class Upgrade {
    constructor(name, cost, costAugmentation, max, now, text, tooltip, img) {
        this.name = name;
        this.cost = cost;
        this.costAugmentation = costAugmentation;
        this.max = max;
        this.current = 0;
        this.visible = false;
        this.visibleRatio = 0.7;
        this.text = text;
        this.tooltip = tooltip;
        this.now = now;
        this.tooltipDisplayCount = 0;
        this.img = img;
    }
    displayIt() {
        return ((this.current < this.max) && this.visible); 
    }
    buy() {
        this.current++;
        this.cost = Math.round(this.cost*this.costAugmentation);
        this.visible = false;
    }
    drawTooltip(x,y) {
        let count = this.tooltipDisplayCount;
        let percent = count/15;
        //outline
        context.lineWidth = 1;
        context.strokeStyle = "black";
        context.fillStyle = "rgba(255, 236, 139," + percent + ")";
        context.beginPath();
        context.moveTo(x,y);
        context.lineTo(x-10,y-10);
        context.lineTo(x-10,y-56);
        context.arcTo(x-10,y-60,x-14,y-60,4);
        /*
        context.lineTo(x-296,y-60);
        context.arcTo(x-300,y-60,x-300,y-56,4);
        context.lineTo(x-300,y+56);
        context.arcTo(x-300,y+60,x-296,y+60,4);
        */
        context.lineTo(x-14-282*percent,y-60);
        context.arcTo(x-18-282*percent,y-60,x-18-282*percent,y-56,4);
        context.lineTo(x-18-282*percent,y+56);
        context.arcTo(x-18-282*percent,y+60,x-14-282*percent,y+60,4);

        context.lineTo(x-14,y+60);
        context.arcTo(x-10,y+60,x-10,y+56,4);
        context.lineTo(x-10,y+10);
        context.lineTo(x,y);
        context.stroke();
        context.fill();
        //content of the tooltip
        if(count >= 15) {
            context.fillStyle = "black";
            context.font = "20px kenney_miniregular";
            let tooltip = this.sliceMyTooltip();
            for(let i=0; i<tooltip.length; i++) {
                context.fillText(tooltip[i], x-15-280, y-40 + 23*i, 280);
            }
            //count
            context.fillStyle = "green";
            context.fillText("upgrade no." + (this.current+1), x-295, y+30, 280);
            //warning
            if(!this.now) {
                context.fillStyle = "red";
                context.fillText("(will apply from next build on)", x-295, y+50, 280);
            }
        }
        if(this.tooltipDisplayCount < 15)
            this.tooltipDisplayCount++;
    }
    sliceMyTooltip() {
        let toolSlice = new Array();
        const maxLength = 45;
        const offset = 15;
        let min = 0;
        let max = min + maxLength-offset;
        while(max < this.tooltip.length) {
            if(this.tooltip[max] === " ") {
                toolSlice.push(this.tooltip.slice(min, max));
                min = max + 1;
                max = min + maxLength - offset;
            } else {
                max++;
            }
        }
        toolSlice.push(this.tooltip.slice(min));
        return toolSlice;
    }
}

class PrestigeBrick {
    /**
     * @param {number} x 
     * @param {number} y 
     * @param {boolean} semi 
     * @param {string} title 
     * @param {string} tooltip 
     * @param {number} cost 
     * @param {string} effect 
     * @param {Array} unlock array of strings (titles)
     * @param {boolean} visible default false
     */
    constructor(x,y,semi,title,tooltip,cost,effect,unlock,visible=false) {
        this.x = x;
        this.y = y;
        this.h = 80;
        this.w = 80;
        if(!semi)
            this.w = this.w * 2;
        this.title = title;
        this.tooltip = this.sliceMyTooltip(tooltip);
        this.cost = cost;
        this.effect = effect;
        this.built = false;
        this.unlock = unlock;
        this.visible = visible;
    }
    sliceMyTooltip(tooltip) {
        let toolSlice = new Array();
        const maxLength = 45;
        const offset = 15;
        let min = 0;
        let max = min + maxLength-offset;
        while(max < tooltip.length) {
            if(tooltip[max] === " ") {
                toolSlice.push(tooltip.slice(min, max));
                min = max + 1;
                max = min + maxLength - offset;
            } else {
                max++;
            }
        }
        toolSlice.push(tooltip.slice(min));
        return toolSlice;
    }
    draw() {
        if(this.visible) {
            if(this.built) {
                ctxPrest.fillStyle = "rgba(30, 130, 76, 1)";
            } else {
                ctxPrest.fillStyle = "rgba(228, 233, 237, 1)";
            }
            ctxPrest.fillRect(this.x+2,this.y+2,this.w-4,this.h-4);
            ctxPrest.strokeStyle = "black";
            ctxPrest.lineWidth = 5;
            ctxPrest.strokeRect(this.x,this.y,this.w,this.h);
    
            ctxPrest.fillStyle = "black";
            ctxPrest.font = "30px kenney_miniregular";
            let cost = this.cost;
            if(cost > 10000)
                cost = cost.toExponential();
            ctxPrest.fillText(cost,this.x+5,this.y+70,this.w-10);
        }
    }
    drawTooltip() {
        let x = this.x + this.w/2;
        let y = this.y;
        let w = 200;
        let h = 110;
        const off = 5;
        ctxPHover.strokeStyle = "black";
        ctxPHover.fillStyle = "white";
        ctxPHover.lineWidth = 3;
        ctxPHover.beginPath();
        ctxPHover.moveTo(x,y);
        ctxPHover.lineTo(x-off,y-off);
        ctxPHover.lineTo(x-w,y-off);
        ctxPHover.lineTo(x-w,y-h);
        ctxPHover.lineTo(x+w,y-h);
        ctxPHover.lineTo(x+w,y-off);
        ctxPHover.lineTo(x+off,y-off);
        ctxPHover.lineTo(x,y);
        ctxPHover.stroke();
        ctxPHover.fill();

        ctxPHover.fillStyle = "black";
        ctxPHover.font = "25px kenney_miniregular";
        for(let i=0; i<this.tooltip.length+1; i++) {
            let text = "";
            if(i===0)
                text = this.title + ":";
            else
                text = this.tooltip[i-1];
            ctxPHover.fillText(text,x-w+off,y-h+22+i*25,2*(w-off));
        }
    }
}

/*------------------------------
Handlers
*/

var gameHandler = {
    state: undefined,
    money: 0,
    brickValue: 1,
    amIWorking: false,
    loading: true,
    timerToAutoRebuild: 0,
    canAutoRebuild: false,
    prestige: false,
    /*
        draw
        validateConstruction
        construct
        finishConstruction
    */

    initialize: function() {
        initializeImg();
        UIHandler.initialize();
        drawHandler.initialize();
        workHandler.initialize();
        brickHandler.initialize();
        scaffoldHandler.initialize();
        upgradeHandler.initialize();
        soundHandler.initialize();
        buildingHandler.initialize();

        this.state = "draw";
        this.money = 10;
        this.brickValue = 1;
        this.amIWorking = false;
        this.loading = true;

        if(this.prestige) {
            prestigeHandler.applyEffects();
            this.prestige = false;
        } else {
            prestigeHandler.initialize();
            saveHandler.initialize();
        }
    },
    calculateValue: function() {
        return brickHandler.brickList.length * this.brickValue;
    },
    nextBuild(auto = false) {
        if(auto) {
            this.canAutoRebuild = false;

            brickHandler.unbuildAll();
            scaffoldHandler.unbuildAll();

            workHandler.reinit();
            brickHandler.autoInit();
            scaffoldHandler.autoInit();

            this.state = "construct";

            UIHandler.drawFirstPaint();
            ctxBrick.clearRect(0,0,canvasBrick.width,canvasBrick.height);
        } else {
            upgradeHandler.effectNow();

            UIHandler.initialize();
            workHandler.reinit();
            drawHandler.initialize();
            brickHandler.initialize();
            scaffoldHandler.initialize();

            this.state = "draw";

            UIHandler.drawFirstPaint();
            ctxBrick.clearRect(0,0,canvasBrick.width,canvasBrick.height);
        }
    },

    core: function() {
        this.amIWorking = true;
        if(this.loading) {
            if(imgLoading.current === imgLoading.total) {
                this.loading = false; 
                UIHandler.drawFirstPaint();
                brickHandler.draw();
                if(this.state !== "finishConstruction")
                    scaffoldHandler.draw();
            } else {
                ctxBG.clearRect(0,0,1300,700);
                ctxBG.fillStyle = "white";
                ctxBG.font = "50px kenney_miniregular";
                ctxBG.fillText("Loading assets (" + imgLoading.current + "/" + imgLoading.total + ")", 325, 400);
            }
        } else {
            if(this.state == "construct") {
                workHandler.core();
                if(brickHandler.brickList.filter(e => e.built == false).length == 0)
                    brickHandler.buildingFinished = true;
                if(brickHandler.buildingFinished) {
                    this.state = "finishConstruction";
                    this.money += this.calculateValue();
                    ctxScaffold.clearRect(0,0,canvasScaffold.width,canvasScaffold.height);
                    if(upgradeHandler.upgradeToCome.length === 0) {
                        this.timerToAutoRebuild = 500;
                        this.canAutoRebuild = true;
                    }
                }
            } else if(this.state == "finishConstruction" && this.canAutoRebuild) {
                this.timerToAutoRebuild--;
                if(upgradeHandler.upgradeToCome.length > 0) {
                    this.canAutoRebuild = false;
                } else if(this.timerToAutoRebuild <= 0) {
                    this.nextBuild(true);
                }
            }
            drawScreen();
            saveHandler.save();
            upgradeHandler.actualiseVisibility();
        }
        this.amIWorking = false;
    }
};

var workHandler = {
    workerList: new Array(),
    cooldown: 100,
    speed: 1,
    unemployed: 0,
    width: 64,
    height: 128,
    startCost: 10,
    cost: 10,
    costAugmentation: 1.6,

    initialize: function() {
        this.workerList = new Array();
        this.cooldown = 100;
        this.speed = 1;
        this.unemployed = 3;
        this.addWorker("miner");
        this.addWorker("builder");
        this.addWorker("lumberjack");
        this.width = 64;
        this.height = 128;
        this.cost = 10;
        this.startCost = 10;
        this.costAugmentation = 1.6;
    },
    reinit: function() {
        let num = {};
        Object.assign(num, this.getNumberWorkers());
        this.workerList = new Array();
        Object.keys(num).forEach(key => {
            if(key != "unemployed") {
                for(let i=0; i<num[key]; i++) {
                    this.workerList.push(new LambdaWorker(key, this.cooldown, this.speed, this.width, this.height, 10*i));
                }
            }
        });
    },
    draw: function() {
        for(let i=0; i<this.workerList.length; i++) {
            this.workerList[i].draw();
        }
    },
    getNumberWorkers: function() {
        let num = {unemployed: this.unemployed, miner: 0, builder: 0, lumberjack: 0};
        for(let i=0; i < this.workerList.length; i++) {
            num[this.workerList[i].job]++;
        }
        return num;
    },
    addWorker: function(job, quantity = 1) {
        if(this.unemployed >= quantity) {
            this.unemployed -= quantity;
            for(let i=0; i<quantity; i++) {
                this.workerList.push(new LambdaWorker(job, this.cooldown, this.speed, this.width, this.height));
            }
        }
    },
    withdrawWorker: function(job, quantity = 1) {
        let num = this.getNumberWorkers();
        if(num[job] >= quantity) {
            for(let j=0; j<quantity; j++) {
                let i=0;
                while(this.workerList[i].job != job) i++;
                w = this.workerList[i];
                if(w.curGoal?.name == "brick") {
                    w.curGoal.brick.flag = undefined;
                    brickHandler.allConstructed = false;
                } else if(w.curGoal?.name == "scaffold") {
                    w.curGoal.scaffold.flag = undefined;
                }
                this.workerList = this.workerList.filter(e => e != w);
            }

            this.unemployed += quantity;
        }
    },
    buyWorker: function(quantity = 1) {
        let cost = this.costXWorkers(quantity);
        if(cost <= gameHandler.money) {
            gameHandler.money -= this.cost;
            this.cost = cost;
            this.cost = Math.round(this.cost * this.costAugmentation);
            this.unemployed += quantity;
        }
    },
    effectAllWorkers: function() {
        for(let i=0; i<this.workerList.length; i++) {
            this.workerList[i].speed = this.speed;
            this.workerList[i].maxCooldown = this.cooldown;
        }
    },
    changeCost: function() {
        let numApply = this.unemployed + this.workerList.length;
        this.cost = this.startCost;
        for(let i=3; i<numApply; i++) {
            this.cost = Math.round(this.cost*this.costAugmentation);
        }
    },
    /**
     * tell me how much the next X workers cost
     * @param {num} numToBuy
     * @return {num} final cost
     */
    costXWorkers(numToBuy) {
        let cost = this.cost;
        let total = 0;
        for(let i=0; i<numToBuy; i++) {
            total += cost;
            cost = Math.round(cost * this.costAugmentation);
        }
        return total;
    },

    core: function() {
        for(let i=0; i<this.workerList.length; i++) {
            let w = this.workerList[i]; 
            if(w.curGoal === undefined) {
                //aller chercher ressource: rock or lumber?
                if(!brickHandler.noNextBrick()) {
                    if(w.transporting === undefined) {
                        w.curGoal = brickHandler.nextBrickBuild();
                        if(w.curGoal !== undefined) {
                            if((w.curGoal.y >= w.goal.mine.y && w.curGoal.y <= w.goal.mine.y + w.height) || (scaffoldHandler.locateLadder(w.curGoal.x, w.curGoal.y) !== undefined)) {
                                w.curGoal.brick.flag = undefined;
                                w.curGoal = w.goal.stockRock;
                            } else {
                                w.curGoal.brick.flag = undefined;
                                w.curGoal = w.goal.stockLumber;
                            }
                        }
                    } else if(w.transporting == "rock") {
                        w.curGoal = brickHandler.nextBrickBuild();
                        if(w.curGoal !== undefined) {
                            if(!((w.curGoal.y >= w.goal.mine.y && w.curGoal.y <= w.goal.mine.y + w.height) || (scaffoldHandler.locateLadder(w.curGoal.x, w.curGoal.y) !== undefined))) {
                                w.curGoal.brick.flag = undefined;
                                w.transporting = undefined;
                                w.curGoal = undefined;
                                brickHandler.stocked++;
                            }
                        }
                    } else if(w.transporting == "lumber") {
                        w.curGoal = scaffoldHandler.nextScaffoldBuild();
                        if(w.curGoal === undefined) {
                            scaffoldHandler.stocked += scaffoldHandler.cost;
                            w.transporting = undefined;
                        }
                    }
                }
            } else if(Math.abs(w.curGoal.x - w.x) < w.speed && w.curGoal.y >= w.y && w.curGoal.y <= w.y + w.height) {
                switch(w.job) {
                    case "miner":
                        switch(w.curGoal.name) {
                            case "mine":
                                w.currentCooldown = w.maxCooldown;
                                w.transporting = "rock";
                                w.curGoal = w.goal.stockRock;
                                break;
                            case "stockRock":
                                w.transporting = undefined;
                                w.curGoal = w.goal.mine;
                                brickHandler.stocked++;
                                break;
                        }
                        break;
                    case "builder":
                        switch(w.curGoal.name) {
                            case "stockRock":
                                if(brickHandler.stocked > 0) {
                                    w.transporting = "rock";
                                    brickHandler.stocked--;
                                }
                                w.curGoal = undefined;
                                break;
                            case "stockLumber":
                                if(scaffoldHandler.stocked >= scaffoldHandler.cost) {
                                    w.transporting = "lumber";
                                    scaffoldHandler.stocked -= scaffoldHandler.cost;
                                }
                                w.curGoal = undefined;
                                break;
                            case "brick":
                                brickHandler.buildBrick(w.curGoal.brick);
                                w.transporting = undefined;
                                w.curGoal = undefined;
                                break;
                            case "scaffold":
                                scaffoldHandler.buildScaffold(w.curGoal.scaffold);
                                w.transporting = undefined;
                                w.curGoal = undefined;
                        }
                        break;
                    case "lumberjack":
                        switch(w.curGoal.name) {
                            case "forest":
                                w.currentCooldown = w.maxCooldown;
                                w.transporting = "lumber";
                                w.curGoal = w.goal.stockLumber;
                                break;
                            case "stockLumber":
                                w.transporting = undefined;
                                w.curGoal = w.goal.forest;
                                scaffoldHandler.stocked++;
                                break;
                        }
                }
            } else if(w.currentCooldown == 0) {
                //déplacement
                if(w.curGoal.x < w.x) w.turn("left");
                if(w.curGoal.x > w.x) w.turn("right");
                if(w.nextFloor === undefined && (w.curGoal.y >= w.y + w.height || w.curGoal.y < w.y)) {
                    //trouver l'emplacement de l'échelle
                    let goal;
                    if(w.curGoal.y >= w.y + w.height) {
                        goal = scaffoldHandler.locateLadder(w.x, w.y);
                        //parfois ne trouve pas l'échelle à un pixel près
                        if(goal == undefined)
                            goal = scaffoldHandler.locateLadder(w.x+w.speed, w.y);
                        if(goal == undefined)
                            goal = scaffoldHandler.locateLadder(w.x-w.speed, w.y)
                    } else {
                        goal = scaffoldHandler.locateLadder(w.curGoal.x, w.curGoal.y);
                    }
                    goal -= Math.round(w.width/2);
                    //s'y diriger
                    if(goal < w.x) w.turn("left");
                    if(goal > w.x) w.turn("right");
                    //si on est sur l'emplacement, monter ou descendre
                    if(Math.abs(goal-w.x) < w.speed) {
                        if(w.curGoal.y >= w.y + w.height) {
                            w.nextFloor = w.y + w.height;
                            w.turn("bottom");
                        } else {
                            w.nextFloor = w.y - w.height;
                            w.turn("top");
                        }
                    }
                } else if(w.nextFloor != undefined) {
                    if(Math.abs(w.y - w.nextFloor) < w.speed) {
                        w.y = w.nextFloor;
                        w.turn("left");
                        w.nextFloor = undefined;
                    } else {                        
                        if(w.y > w.nextFloor) w.turn("top");
                        else w.turn("bottom");
                    }
                }
                w.move();
            } else {
                w.currentCooldown--;
            }
        }
    }
};

var UIHandler = {
    drawSurface: {x1: 300, y1: 50, x2: 1000, y2: 550},
    zoomLevel: 1,
    x1: 500,
    x2: 800,
    y1: 350,
    y2: 550,
    mine: undefined,
    stockRock: undefined,
    forest: undefined,
    stockLumber: undefined,
    menuWorker: false,
    menuUpgrade: false,
    menuHelp: false,
    menuOption: false,
    menuErase: false,
    menuImpExp: false,
    menuPrestige: false,
    numToBuy: 1,

    firstInit: function() {
        this.zoomLevel = 1;
        this.x1 = 500;
        this.x2 = 800;
        this.y1 = 350;
        this.y2 = 550;
    },
    initialize: function() {
        let w = Math.floor((this.x2 - this.x1)/workHandler.width)*workHandler.width;
        let x = this.x1 + Math.floor((this.x2-this.x1-w)/2) ;
        let square = Math.floor(130/this.zoomLevel);
        this.drawSurface = {x: x, y: this.y1, w: w, h: this.y2-this.y1};
        this.mine = {x: 0, y: 550-square, w: square, h: square, color: "grey"};
        this.stockRock = {x: x-square, y: 550-square, w: square, h: square, color: "red"};
        this.forest = {x: 1300-square, y: 550-square, w: square, h: square, color: "green"};
        this.stockLumber = {x: x+w, y: 550-square, w: square, h: square, color: "orange"};
        this.menuWorker = false;
        this.menuUpgrade = false;
        this.menuHelp = false;
        this.menuOption = false;
        this.menuErase = false;
        this.menuPrestige = false;
    },
    clic: function() {
        let x = mouse.x;
        let y = mouse.y;
        let clicEffect = false;
        if(!clicEffect && gameHandler.state == "finishConstruction") {
            if(x >= 475 && x <= 645 && y >= 620 && y <= 690) {
                buildingHandler.addNewBuilding();
                gameHandler.nextBuild();
                clicEffect = true;
            }
        }
        if(!clicEffect && gameHandler.state == "validateConstruction") {
            if(x >= 400 && x <= 600 && y >= 200 && y <= 250) {
                drawHandler.validate(false);
                clicEffect = true;
            }
            if(x >= 700 && x <= 900 && y >= 200 && y <= 250) {
                drawHandler.validate(true);
                clicEffect = true;
            }
        }
        if(!clicEffect && this.menuPrestige) {
            if(x >= 1195 && x <= 1235 && y >= 65 && y <= 105) {
                this.menuPrestige = false;
                prestigeHandler.clearDraw();
                clicEffect = true;
            }
            if(prestigeHandler.clic(x,y)) {
                clicEffect = true;
            }
        }
        if(!clicEffect && x >= 20 && x <= 220 && y >= 20 && y <= 90) {
            this.menuWorker = !this.menuWorker;
            clicEffect = true;
        }
        if(!clicEffect && x >= canvas.width - 220 && x <= canvas.width - 20 && y >= 20 && y <= 90) {
            if(!this.menuUpgrade)
                upgradeHandler.newUpgrade = false;
            this.menuUpgrade = !this.menuUpgrade;
            upgradeHandler.page = 1;
            clicEffect = true;
        }
        if(!clicEffect && this.menuWorker) {
            if(y >= 140 && y <= 180) {
                if(x >= 35 && x <= 65) {
                    this.numToBuy = 1;
                    clicEffect = true;
                } else if(x >= 70 && x <= 100) {
                    this.numToBuy = 5;
                    clicEffect = true;
                } else if(x >= 105 && x <= 150) {
                    this.numToBuy = 25;
                    clicEffect = true;
                } else if(x >= 155 && x <= 205) {
                    this.numToBuy = 100;
                    clicEffect = true;
                }
            } else if(x >= 30 && x <= 50 && y >= 230 && y <= 250) {
                workHandler.buyWorker(this.numToBuy);
                clicEffect = true;
            } else if(x >= 90 && x <= 110) {
                if(y >= 330 && y <= 350) {
                    workHandler.withdrawWorker("miner", this.numToBuy);
                    clicEffect = true;
                } else if(y >= 400 && y <= 420) {
                    workHandler.withdrawWorker("builder", this.numToBuy);
                    clicEffect = true;
                } else if(y >= 470 && y <= 490) {
                    workHandler.withdrawWorker("lumberjack", this.numToBuy);
                    clicEffect = true;
                }
            } else if(x >= 130 && x <= 150) {
                if(y >= 330 && y <= 350) {
                    workHandler.addWorker("miner", this.numToBuy);
                    clicEffect = true;
                } else if(y >= 400 && y <= 420) {
                    workHandler.addWorker("builder", this.numToBuy);
                    clicEffect = true;
                } else if(y >= 470 && y <= 490) {
                    workHandler.addWorker("lumberjack", this.numToBuy);
                    clicEffect = true;
                }
            }
        }
        if(!clicEffect && this.menuUpgrade) {
            if(upgradeHandler.clic())
                clicEffect = true;
        }
        if(!clicEffect && x >= 1230 && x <= 1280 && y >= 630 && y <= 680) {
            //option
            this.menuOption = !this.menuOption;
            if(this.menuOption) {
                this.menuHelp = false;
            }
            else {
                this.menuImpExp = false;
                closeImportExport();
            }
            clicEffect = true;
        }
        if(!clicEffect && x >= 1110 && x <= 1160 && y >= 630 && y <= 680) {
            soundHandler.changeHearabilityMusic();
            clicEffect = true;
        }
        if(!clicEffect && this.menuHelp && x >= 930 && x <= 980 && y >= 100 && y <= 150) {
            this.menuHelp = !this.menuHelp;
            clicEffect = true;
        }
        if(!clicEffect && this.menuOption) {
            if(x >= 930 && x <= 980 && y >= 100 && y <= 150) {
                this.menuOption = !this.menuOption;
                this.menuErase = false;
                closeImportExport();
                this.menuImpExp = false;
                clicEffect = true;
            } else if(y >= 440 && y <= 490 && !this.menuImpExp) {
                if(x >= 500 && x <= 640) {
                    this.menuImpExp = true;
                    saveHandler.import();
                    clicEffect = true;
                } else if(x >= 660 && x <= 800) {
                    this.menuImpExp = true;
                    saveHandler.export();
                    clicEffect = true;
                }
            }
        }
        if(!clicEffect && this.menuOption && !this.menuImpExp && x >= 410 && x <= 910 && y >= 520 && y <= 560) {
            if(!this.menuErase) {
                this.menuErase = true;
            } else {
                saveHandler.erase();
                this.menuErase = false;
            }
            clicEffect = true;
        }
        if(!clicEffect && x >= 1170 && x <= 1220 && y >= 630 && y <= 680) {
            //help
            this.menuHelp = !this.menuHelp;
            if(this.menuHelp) {
                this.menuOption = false;
                this.menuErase = false;
            }
            clicEffect = true;
        }
        if(!clicEffect && gameHandler.state == "construct" && x >= 565 && x <= 735 && y >= 620 && y <= 690) {
            brickHandler.buildingFinished = true;
            brickHandler.brickList = brickHandler.brickList.filter(e => e.built);
            clicEffect = true;
        }
        if(!clicEffect && prestigeHandler.unlocked && x >= 20 && x <= 220 && y >= 620 && y <= 690) {
            this.menuPrestige = !this.menuPrestige;
            if(!this.menuPrestige) {
                prestigeHandler.confirmRestart = false;
                prestigeHandler.clearDraw();
            }
            clicEffect = true;
        }
    },
    drawFirstPaint: function() {
        ctxBG.fillStyle = 'RGBA(173, 216, 230, 1)';
        ctxBG.fillRect(0, 0, canvasBackground.width, canvasBackground.height);
        this.drawBackground();
        //mine
        ctxBG.drawImage(mine, this.mine.x, this.mine.y, this.mine.w, this.mine.h);
        //forest
        ctxBG.drawImage(forest, this.forest.x, this.forest.y, this.forest.w, this.forest.h);
        //dessous: sol
        ctxBG.fillStyle = 'RGBA(166, 123, 91, 1)';
        ctxBG.fillRect(0, this.drawSurface.h+this.drawSurface.y, canvas.width, canvas.height - this.drawSurface.h);
        for(i =0; i<1300; i+=100) {
            for(j=550; j<=700; j+=75) {
                ctxBG.drawImage(ground2, i, j);
            }            
            ctxBG.drawImage(ground, i, 540);
        }
    },
    drawSecondPaint: function() {
        //validation d'un dessin
        if(gameHandler.state == "validateConstruction") {
            this.drawButton(400,200,200,50,true,"red",ctxFront);
            this.drawButton(700,200,200,50,true,"green",ctxFront);
        
            ctxFront.fillStyle = "black";
            ctxFront.font = "20px kenney_miniregular";
            ctxFront.fillText("Redo the drawing", 412, 232, 186);
            ctxFront.fillText("Begin Construction", 709, 232, 185);
        }
    },
    drawConstructInfos: function() {
        //stockrock
        let h = Math.round(this.stockRock.h/6);
        let w = Math.round(this.stockRock.w/3);
        let x = this.stockRock.x;
        let y = this.stockRock.y + this.stockRock.h - h;
        ctxFront.fillStyle = "grey";
        ctxFront.strokeStyle = "black";
        let rockNum = 0;
        let rockMax = brickHandler.stocked;
        let i = 0;
        let j = 0;
        ctxFront.lineWidth = 1;
        let altern=false;
        let offset=Math.round(w/2);
        while(rockNum < rockMax && j < 6) {
            ctxFront.fillRect(x+w*i+altern*offset,y-h*j,w,h);
            ctxFront.strokeRect(x+w*i+altern*offset,y-h*j,w,h);
            rockNum++;
            i++;
            if(i == 3 || (altern && i == 2)) {
                i=0;
                j++;
                altern = !altern;
            }
        }
        //stocklumber
        h = Math.round(this.stockLumber.h/8);
        w = Math.round(this.stockLumber.w/2);
        x = this.stockLumber.x;
        y = this.stockLumber.y + this.stockLumber.h - h;
        ctxFront.fillStyle = "rgba(164, 116, 73, 1)";
        ctxFront.strokeStyle = "black";
        let lumberNum = 0;
        let lumberMax = scaffoldHandler.stocked;
        i = 0;
        j = 0;
        ctxFront.lineWidth = 1;
        while(lumberNum < lumberMax && j < 8) {
            ctxFront.fillRect(x+w*i,y-h*j,w,h);
            ctxFront.strokeRect(x+w*i,y-h*j,w,h);
            lumberNum++;
            i++;
            if(i == 2) {
                i=0;
                j++;
            }
        }

        //option terminer immédiatement
        this.drawButton(565,620,170,70,true,"green",ctxFront);
        ctxFront.fillStyle = "black";
        ctxFront.font = "30px kenney_miniregular";
        ctxFront.fillText("finish early", 575, 663, 150);
    },
    drawNumberInfos: function() {
        //informations
        this.drawButton(200,560,200,50,false,"grey",ctxFront);
        this.drawButton(550,560,200,50,false,"yellow",ctxFront);
        this.drawButton(900,560,200,50,false,"brown",ctxFront);
        ctxFront.fillStyle = "black";
        ctxFront.font = "30px kenney_miniregular";
        ctxFront.fillText("bricks: " + brickHandler.stocked, 210, 595, 180);
        ctxFront.fillText("lumber: " + scaffoldHandler.stocked, 910, 595, 180);
        ctxFront.fillText("gold: " + gameHandler.money, 560, 595, 180);
    },
    drawMenu() {
        //menus workers/upgrades
        let x2 = canvas.width - 220;
        if(workHandler.unemployed > 0)
            this.drawButton(20,20,200,70,true,"red",ctxFront);
        else
            this.drawButton(20,20,200,70,true,"yellow",ctxFront);
        if(upgradeHandler.newUpgrade)
            this.drawButton(x2,20,200,70,true,"red",ctxFront);
        else 
            this.drawButton(x2,20,200,70,true,"yellow",ctxFront);

        ctxFront.fillStyle = "black";
        ctxFront.font = "30px kenney_miniregular";
        ctxFront.fillText("Workers", 50, 65);
        ctxFront.fillText("Upgrades", canvas.width - 195, 65);

        //bouton aide/musique
        ctxFront.drawImage(imgMenu.help, 1170, 630);
        ctxFront.drawImage(imgMenu.option, 1230, 630);
        if(soundHandler.musicOn)
            ctxFront.drawImage(imgMenu.musicOn, 1110, 630);
        else
            ctxFront.drawImage(imgMenu.musicOff, 1110, 630);

        //sauvegarde
        if(saveHandler.countToSave > 460)
            ctxFront.drawImage(imgMenu.save, 1050, 630);

        //prestige
        if(prestigeHandler.unlocked) {
            this.drawButton(20,620,200,70,true,"red",ctxFront);
            ctxFront.fillStyle = "black";
            ctxFront.fillText("Prestige", 55, 665);
        }

        if(this.menuWorker) {
            //cadre
            this.drawButton(20,100,200,430,true,"yellow2",ctxFront);
            //choix du nombre à acheter
            let color = "lightgrey";
            if(this.numToBuy === 1)
                color = "red";
            this.drawButton(35,140,30,40,false,color,ctxFront);
            if(this.numToBuy === 5)
                color = "red"; 
            else 
                color = "lightgrey";
            this.drawButton(70,140,30,40,false,color,ctxFront);
            if(this.numToBuy === 25)
                color = "red"; 
            else 
                color = "lightgrey";
            this.drawButton(105,140,45,40,false,color,ctxFront);
            if(this.numToBuy === 100)
                color = "red"; 
            else 
                color = "lightgrey";
            this.drawButton(155,140,50,40,false,color,ctxFront);

            ctxFront.fillStyle = "black";
            ctxFront.font = "20px kenney_miniregular";
            ctxFront.fillText("Amount to change: ", 30, 130, 180);

            ctxFront.fillText("1", 47, 167);
            ctxFront.fillText("5", 80, 167);
            ctxFront.fillText("25", 115, 167);
            ctxFront.fillText("100", 165, 167);
            //liste des travailleurs
            let numWorkers = workHandler.getNumberWorkers();
            ctxFront.font = "25px kenney_miniregular"; 
            ctxFront.fillStyle = "black";
            ctxFront.fillText("Unemployed: " + numWorkers.unemployed, 30, 210, 180)
            ctxFront.fillText("Miners: "  + numWorkers.miner, 30, 310, 180);
            ctxFront.fillText("Builders: "  + numWorkers.builder, 30, 380, 180);
            ctxFront.fillText("Lumberjacks: "  + numWorkers.lumberjack, 30, 450, 180);
            //boutons
            ctxFront.fillStyle = 'white';
            ctxFront.strokeStyle = 'black';
            ctxFront.lineWidth = 1;
            for(let i=90; i<=130; i+=40) {
                for(let j=330; j<=470; j+=70) {
                    ctxFront.fillRect(i,j,20,20);
                    ctxFront.strokeRect(i,j,20,20);
                    ctxFront.strokeRect(i+3,j+10,14,1);
                    if(i==130) {
                        ctxFront.strokeRect(i+10,j+3,1,14);
                    }
                }
            }
            ctxFront.fillRect(30,230,20,20);
            ctxFront.strokeRect(30,230,20,20);
            ctxFront.strokeRect(33,240,14,1);
            ctxFront.strokeRect(40,233,1,14);
            ctxFront.fillStyle = "black";
            ctxFront.font = "20px kenney_miniregular";
            let cost =  workHandler.costXWorkers(this.numToBuy);
            if(cost >= 1e10) {
                cost = "too expensive";
            }
            ctxFront.fillText("cost: " + cost, 60, 248, 150);
        }
        if(this.menuUpgrade) {
            let x2 = canvas.width - 220;
            this.drawButton(x2,100,200,430,true,"yellow2",ctxFront);
            upgradeHandler.draw();
        }
        if(this.menuOption) {
            //cadre
            this.drawButton(300,80,700,500,true,"lightgrey",ctxFront);
            //croix
            ctxFront.lineWidth = 10;
            ctxFront.strokeStyle = "rgba(150, 40, 27, 1)";
            ctxFront.beginPath();
            ctxFront.moveTo(930,100);
            ctxFront.lineTo(980,150);
            ctxFront.moveTo(980,100);
            ctxFront.lineTo(930,150);
            ctxFront.stroke();
            //erase save
            ctxFront.fillStyle = "black";
            ctxFront.font = "30px kenney_miniregular";
            if(!this.menuErase) {
                ctxFront.fillText("Reset the game (will erase the save)", 365, 550);
            } else {
                ctxFront.fillStyle = "red";
                ctxFront.fillText("Are you sure? (this is definitive)", 400, 550);
            }
            //credits
            //Credit (Kenney or www.kenney.nl) for assets
            ctxFront.font = "50px kenney_miniregular";
            ctxFront.fillStyle = "black";
            ctxFront.fillText("CASTLE BUILDERS", 350, 150);
            ctxFront.font = "30px kenney_miniregular";
            ctxFront.fillText("v1.1 2021 - code by Robin", 350, 200);
            ctxFront.fillText("Workers img by Kenney ( www.kenney.nl )", 350, 250);
            ctxFront.fillText("Music by Anttis", 350, 300);
            ctxFront.fillText("(http://anttismusic.blogspot.com/)", 400, 350);
            ctxFront.fillText("Save every 5 seconds", 480, 420);
            this.drawButton(500,440,140,50,true,"grey",ctxFront);
            this.drawButton(660,440,140,50,true,"grey",ctxFront);
            ctxFront.fillStyle = "black";
            ctxFront.fillText("Import", 520, 474);
            ctxFront.fillText("Export", 677, 474);
        }
        if(this.menuHelp) {
            //cadre
            this.drawButton(300,80,700,500,true,"lightgrey",ctxFront);
            //croix
            context.lineWidth = 10;
            context.strokeStyle = "rgba(150, 40, 27, 1)";
            context.beginPath();
            context.moveTo(930,100);
            context.lineTo(980,150);
            context.moveTo(980,100);
            context.lineTo(930,150);
            context.stroke();
            //erase save
            context.fillStyle = "black";
            context.font = "50px kenney_miniregular";
            context.fillText("HOW TO PLAY:", 350, 150);
            context.font = "30px kenney_miniregular";
            context.fillText("> draw the shape you want to build", 350, 200);
            context.fillText("> assign roles to your workers", 350, 250);
            context.fillText("> miners and lumberjacks refill stocks", 350, 300);
            context.fillText("> builder can only reach so far", 350, 350);
            context.fillText("   so they will need scaffolds", 350, 400);
            context.fillText("> scaffolds = 5 lumber, bricks = 1 rock", 350, 450);
            context.fillText("> finish a building to earn money", 350, 500);
            context.fillText("   dependent on the bricks used", 350, 550);
        }
        if(this.menuPrestige) {
            prestigeHandler.draw();
            prestigeHandler.drawInfos();
        }
    },
    drawBuildingFinished() {
        ctxFront.font = "30px kenney_miniregular";
        ctxFront.fillStyle = "black";
        ctxFront.fillText("You finished this building and earned " + gameHandler.calculateValue() + " gold !", 300, 35);

        this.drawButton(655,620,170,70,false,"grey",ctxFront);
        this.drawButton(475,620,170,70,true,"green",ctxFront);
        ctxFront.fillStyle = "black";
        ctxFront.fillText("next build", 485, 663);
        if(gameHandler.canAutoRebuild)
            ctxFront.fillText("restart (" + Math.round(gameHandler.timerToAutoRebuild/100) + "s)", 665, 663, 153);
        else
            ctxFront.fillText("can't rebuild", 665, 663, 153);
        let listUp = upgradeHandler.arrangeList();
        if(listUp.length > 0)
            ctxFront.fillText("List of upgrades from next build on:", 350, 70);
        for(let i=0; i<listUp.length; i++) {
            let up = listUp[i];
            let str = up.text;
            if(up.num>1) str += " x" + up.num;
            ctxFront.fillText("- " + str, 350, 105 + 35*i);
        }
    },
    drawBackground() {
        let i,j;
        let zoom = this.zoomLevel;
        //sky
        ctxBG.drawImage(imgBK.sky, 0, 0);
        ctxBG.drawImage(imgBK.sky, 1024, 0);
        //fill green
        ctxBG.fillStyle = "rgba(168,215,185,1)";
        ctxBG.fillRect(0,320+40*(zoom-1),1300,300);
        //clouds : 1000x206
        j = 50 + (zoom-1)*170 - (zoom>2)*80;
        for(i=0; i<=1300; i+=1000/zoom) {
            ctxBG.drawImage(imgBK.cloud1, i, j, 1000/zoom, 206/zoom);
        }
        //mountains : 1000x168
        j = 140 + (zoom-1) *125 - (zoom>2)*50;
        for(i=0; i<=1300; i+=1000/zoom) {
            ctxBG.drawImage(imgBK.mountain, i, j, 1000/zoom, 140/zoom);
        }
        //hills : 1000x128
        j = 240 + (zoom -1)*65;
        for(i=0; i<=1300; i+=1000/zoom) {
            ctxBG.drawImage(imgBK.hill2, i, j, 1000/zoom, 128/zoom);
        }
        //buildings
        buildingHandler.draw();
        j = 280 + (zoom -1)*50; 
        for(i=-300 + (zoom-1)*100; i<=1300; i+=1000/zoom) {
            ctxBG.drawImage(imgBK.hill1, i, j, 1000/zoom, 128/zoom);
        }
        //trees
    },
    /**
     * draw a button at position specified
     * @param {num} x left
     * @param {num} y top
     * @param {num} w width
     * @param {num} h height
     * @param {bool} clickable if true, show little corners 
     * @param {string} color background color (default yellow)
     * @param {context} ctxParam layer onto we draw (default to front)
     */
    drawButton(x,y,w,h,clickable,color,ctxParam = ctxFront) {
        ctxParam.lineWidth = 3;
        ctxParam.fillStyle = "rgba(150, 40, 27, 1)";
        ctxParam.strokeStyle = "black";
        ctxParam.fillRect(x, y, w, h);
        ctxParam.strokeRect(x, y, w, h);
        let rgba;
        switch(color) {
            case "red":
                rgba = "rgba(240, 52, 52, 1)";
                break;
            case "green":
                rgba = "rgba(30, 130, 76, 1)";
                break;
            case "grey":
                rgba = "rgba(109, 113, 122, 1)";
                break;
            case "lightgrey":
                rgba = "rgba(228, 233, 237, 1)";
                break;
            case "brown":
                rgba = "rgba(164, 116, 73, 1)";
                break;
            case "yellow2":
                rgba = "rgba(245, 215, 110, 1)";
                break;
            case "yellow":
            default:
                rgba = "rgba(247, 202, 24, 1)";
                break;
        }
        ctxParam.fillStyle = rgba;
        ctxParam.fillRect(x+5, y+5,w-10,h-10);
        ctxParam.strokeRect(x+5, y+5,w-10,h-10);
        if(clickable) {
            ctxParam.fillStyle = "rgba(108, 122, 137, 1)";
            ctxParam.lineWidth = 5;
            ctxParam.beginPath();
            ctxParam.arc(x+2.5,y+2.5,5,0,2*Math.PI);
            ctxParam.stroke();
            ctxParam.fill();
            ctxParam.beginPath();
            ctxParam.arc(x+w-2.5,y+h-2.5,5,0,2*Math.PI);
            ctxParam.stroke();
            ctxParam.fill();
            ctxParam.beginPath();
            ctxParam.arc(x+2.5,y+h-2.5,5,0,2*Math.PI);
            ctxParam.stroke();
            ctxParam.fill();
            ctxParam.beginPath();
            ctxParam.arc(x+w-2.5,y+2.5,5,0,2*Math.PI);
            ctxParam.stroke();
            ctxParam.fill();
        }
    },
};

var upgradeHandler = {
    upgradeList: new Array(),
    upgradeToCome: new Array(),
    page: 1,
    mouseOn: undefined,
    newUpgrade: false,

    /*  liste upgrades:
    -> cooldown -- cooldown travailleurs
    -> costAugmention -- augmentation du cout des travailleurs
    -> cost -- cout de base des travailleurs
    -> speed -- vitesse travailleurs
    -> sizeX -- taille des briques en X
    -> sizeY -- taille des briques en Y
    -> surfaceSize -- taille du terrain
    -> value -- valeur des briques
    -> scaffold -- cout des échafaudages
    */

    initialize: function() {
        this.upgradeList = new Array();
        this.upgradeToCome = new Array();
        this.page = 1;
        this.mouseOn = undefined;
        this.newUpgrade = false;
        //this.upgradeList.push(new Upgrade(name, cost, costAug, max, now, text, tooltip, img));
        this.upgradeList.push(new Upgrade("cooldown", 10, 1.9, 9, true, "Working faster", "Miners and Lumberjacks spend   10 less ticks working          (starting at 100)", "dollar"));
        this.upgradeList.push(new Upgrade("costAugmentation", 15, 2.5, 8, true, "Decrease salary", "Cost of workers increase       0.05 slower (starting at x1.6)", "dollar"));
        this.upgradeList.push(new Upgrade("cost", 10, 4.5, 5, true, "Cheaper workers", "Workers base cost decrease     by 1 (starting at 10 gold)", "dollar"));
        this.upgradeList.push(new Upgrade("speed", 50, 2.2, 9, true, "Displacement speed", "Workers speed increase by 1    (starting at 1)", "speed"));
        this.upgradeList.push(new Upgrade("sizeX", 20, 1.3, 18, false, "Smaller Brick X", "Bricks width decrease by 4     (starting at 100)", "width"));
        this.upgradeList.push(new Upgrade("sizeY", 20, 1.3, 18, false, "Smaller Brick Y", "Bricks height decrease by 2    (starting at 50)", "height"));
        this.upgradeList.push(new Upgrade("surfaceSize", 20, 6, 5, false, "Larger Terrain", "Build terrain width and        height will augment            significatively", "surface"));
        this.upgradeList.push(new Upgrade("value", 10, 1.6, 9, true, "Brick value", "Brick value increase by 1      (starting at 1)", "dollar"));
        this.upgradeList.push(new Upgrade("scaffold", 15, 5.5, 4, true, "Scaffold cost", "Scaffold cost 1 less lumber    to build (starting at 5)", "dollar"));
        this.upgradeList.push(new Upgrade("victory", 100000, 10, 1, true, "Retire", "You've now enough gold to retire. Or continue playing. Either way, you've won.", "dollar"));
    },
    reinit: function() {
        let newList = new Array();
        for(let i=0; i<this.upgradeList.length; i++) {
            let u = this.upgradeList[i];
            let u2 = new Upgrade(u.name, u.cost, u.costAugmentation, u.max, u.now, u.text, u.tooltip, u.img);
            u2.current = u.current;
            u2.visible = u.visible;
            u2.visibleRatio = u.visibleRatio;
            newList.push(u2);
        }
        this.upgradeList = newList;
    },
    clic: function() {
        let x = mouse.x;
        let y = mouse.y;
        if(x >= canvas.width-180 && x <= canvas.width-140 && y >= 110 && y <= 150) {
            //flèche gauche
            if(this.page > 1) {
                this.page--;
            }
            return true;
        } else if(x >= canvas.width-100 && x <= canvas.width-60 && y >= 110 && y <= 150) {
            //flèche droite
            if(this.page < this.listBuyables().length/4) {
                this.page++;
            }
            return true;
        } else {
            let up = this.whichIsConcerned();
            if(up !== undefined) {
                this.buy(up);
                return true;
            }
        }
        return false;
    },
    buy: function(upgrade) {
        if(upgrade.cost <= gameHandler.money) {
            gameHandler.money -= upgrade.cost;
            upgrade.buy();
            if(upgrade.now)
                this.effect(upgrade.name);
            else   
                this.upgradeToCome.push({name: upgrade.name, text: upgrade.text});
        }
    },
    whichIsConcerned: function()  {
        let x = mouse.x;
        let y = mouse.y;
        let upgrade = undefined;
        if(x >= canvas.width-210 && x <= canvas.width-30) {
            let num = undefined;
            for(let i=0; i<4; i++) {
                if(y >= 170+i*85 && y <= 255+i*85) {
                    num = i;
                }
            }
            if(num !== undefined) {
                num = (this.page-1)*4 + num;
                upList = this.listBuyables();
                if(num < upList.length)
                    upgrade = upList[num];
            }
        }
        return upgrade;
    },
    arrangeList: function() {
        let up1 = this.upgradeToCome;
        let up2 = new Array();
        for(let i=0; i<up1.length; i++) {
            let text = up1[i].text;
            let j=0;
            while(j<up2.length && up2[j].text != text) j++;
            if(j < up2.length) up2[j].num++;
            else up2.push({text: text, num: 1});
        }
        return up2;
    },
    effect: function(name) {
        switch(name) {
            case "cooldown":
                workHandler.cooldown -= 10;
                workHandler.effectAllWorkers();
                break;
            case "costAugmentation":
                workHandler.costAugmentation -= 0.05;
                workHandler.changeCost();
                break;
            case "cost":
                workHandler.startCost -= 1;
                workHandler.changeCost();
                break;
            case "speed":
                workHandler.speed++;
                workHandler.effectAllWorkers();
                break;
            case "sizeX":
                brickHandler.sizeX -= 4;
                break;
            case "sizeY":
                brickHandler.sizeY -= 2;
                break;
            case "surfaceSize":
                if(UIHandler.x1 > 308) {
                    UIHandler.x1 -= 64;
                    UIHandler.x2 += 64;
                    UIHandler.y1 -= 100;
                } else {
                    UIHandler.zoomLevel += 1;
                }
                break;
            case "value":
                gameHandler.brickValue++;
                break;
            case "scaffold":
                scaffoldHandler.cost--;
                break;
            case "victory":
                prestigeHandler.unlocked = true;
                break;
        }
    },
    effectNow: function() {
        for(let i=0; i<this.upgradeToCome.length; i++) {
            this.effect(this.upgradeToCome[i].name);
        }
        this.upgradeToCome = new Array();
    },
    mouseHover: function() {
        if(UIHandler.menuUpgrade) {
            let up = this.whichIsConcerned();
            if(up !== undefined && this.mouseOn?.upgrade !== up) {
                let x = canvas.width - 215;
                let y = Math.floor((mouse.y - 170)/85)*85 + 212.5;
                this.mouseOn = {upgrade: up, x: x, y: y};
                up.tooltipDisplayCount = 0;
            } else if(up === undefined) {
                this.mouseOn = undefined;
            }
        } else {
            this.mouseOn = undefined;
        }
    },
    listBuyables: function() {
        let l = new Array();
        for(let i=0; i<this.upgradeList.length; i++) {
            if(this.upgradeList[i].displayIt())
                l.push(this.upgradeList[i]);
        }
        return l;
    },
    draw: function() {
        //taille du cadre
        let x = canvas.width - 220;
        let w = 200;
        let y = 110;
        let h = 430;
        let listUpgrade = this.listBuyables();
        if((this.page-1)*4 >= listUpgrade.length)
            this.page--;
        if(this.page < 1)
            this.page = 1;
        //afficher des flèches de défilement
        if(listUpgrade.length > 4) {
            context.fillStyle = "black";
            context.strokeStyle = "rgba(149, 165, 166, 1)";
            context.lineWidth = 2;
            let x1 = Math.round(x + w/2);
            for(let sens=-1; sens<2; sens+=2) {
                if((sens == -1 && this.page > 1) || (sens == 1 && this.page < listUpgrade.length/4)) {
                    context.beginPath();
                    context.moveTo(x1 + sens*20, y+20);
                    context.lineTo(x1 + sens*40, y+20);
                    context.lineTo(x1 + sens*40, y+10);
                    context.lineTo(x1 + sens*60, y+25);
                    context.lineTo(x1 + sens*40, y+40);
                    context.lineTo(x1 + sens*40, y+30);
                    context.lineTo(x1 + sens*20, y+30);
                    context.lineTo(x1 + sens*20, y+20);
                    context.stroke();
                    context.fill();
                }
            }
        }
        //afficher les upgrades
        let firstToShow = (this.page-1)*4
        let sizeX = w-20;
        let sizeY = 85;        
        for(let i=0; i<listUpgrade.length; i++) {
            if(i >= firstToShow && i < firstToShow+4) {        
                let y1 = y+60+ (i-firstToShow)*sizeY;
                let up = listUpgrade[i];
                context.fillStyle = "rgba(247, 202, 24, 1)";
                context.strokeStyle = "black";
                context.lineWidth = 4;
                context.fillRect(x+10,y1,sizeX,sizeY);
                context.strokeRect(x+10,y1,sizeX,sizeY);
                context.font = "20px kenney_miniregular";
                context.fillStyle = "black";
                context.fillText(up.text, x+15, y1+25, sizeX-10);
                context.fillText("cost:", x+15, y1+50, sizeX-40);
                context.fillText(up.cost, x+15, y1+75, sizeX-40);
                context.drawImage(imgUp[up.img], x+125, y1+35);
            }
        }
    },
    drawTooltip: function() {
        if(this.mouseOn !== undefined) {
            this.mouseOn.upgrade.drawTooltip(this.mouseOn.x, this.mouseOn.y);
        }
    },
    actualiseVisibility: function() {
        for(let i=0; i<this.upgradeList.length; i++) {
            let up = this.upgradeList[i];
            let gold = gameHandler.money;
            if(!up.visible && up.cost*up.visibleRatio < gold) {
                up.visible = true;
                this.newUpgrade = true;
            }
        }
    },
};

var brickHandler = {
    brickList: new Array(),
    stocked: undefined,
    allConstructed: false,
    buildingFinished: false,
    sizeX: 100,
    sizeY: 50,
    goldenChance: 0.01,

    firstInit: function() {
        this.sizeX = 100;
        this.sizeY = 50;
        ctxBrick.clearRect(0,0,1300,700);
        this.goldenChance = 0.01;
    },
    initialize: function() {
        this.brickList = new Array();
        this.stocked = 0;
        this.allConstructed = false;
        this.buildingFinished = false;
    },
    autoInit() {
        this.stocked = 0;
        this.allConstructed = false;
        this.buildingFinished = false;
    },
    reinit: function() {
        let newList = new Array();
        for(let i=0; i<this.brickList.length; i++) {
            let b1 = this.brickList[i];
            let b2 = new Brick(b1.x, b1.y, b1.sizeX, b1.sizeY);
            b2.built = b1.built;
            if(b1.golden !== undefined)
                b2.golden = b1.golden;
            b2.flag = undefined;
            newList.push(b2);
        }
        this.brickList = newList;
    },
    add: function(x,y,sizeX,sizeY) {
        this.brickList.push(new Brick(x,y,sizeX,sizeY));
    },
    draw: function() {
        let allBuilt = (this.brickList.length > 0);
        for(let i=0; i<this.brickList.length; i++) {
            this.brickList[i].draw();
            if(!this.brickList[i].built)
                allBuilt = false;
        }
        if(allBuilt)
            this.buildingFinished = true;
    },
    nextBrickBuild: function() {
        let b = undefined;
        for(let i=0; i<this.brickList.length; i++) {
            let b2 = this.brickList[i];
            if(!b2.built && b2.flag === undefined) {
                if(b === undefined || b2.y > b.y)
                    b = b2;
            }
        }
        if(b !== undefined) {
            b.flag = "taken";
            return {x: b.x, y: b.y, name: "brick", brick: b};
        } else {
            this.allConstructed = true;
            return undefined;
        }
    },
    buildBrick: function(brick) {
        brick.built = true;
        if(Math.random() < this.goldenChance) {
            brick.golden = true;
            prestigeHandler.addGoldenBrick();
        }
        brick.draw();
    },
    noNextBrick: function() {
        return this.allConstructed && this.buildingFinished;
    },
    noBrickHere: function(x,y) {
        for(let i=0; i<this.brickList.length; i++) {
            let b = this.brickList[i];
            if(x >= b.x && x <= b.x + b.sizeX && y >= b.y && y <= b.y + b.sizeY) {
                return false;
            }
        }
        return true;
    },
    unbuildAll() {
        for(let i=0; i<this.brickList.length; i++) {
            this.brickList[i].built = false;
            this.brickList[i].flag = undefined;
        }
    },
};

var scaffoldHandler = {
    scaffoldList: new Array(),
    allConstructed: false,
    stocked: undefined,
    cost: 5,
    sizeX: undefined,
    sizeY: undefined,

    firstInit: function() {
        this.cost = 5;
        ctxScaffold.clearRect(0,0,1300,700);
    },
    initialize: function() {
        this.scaffoldList = new Array();
        this.allConstructed = false;
        this.stocked = 0;
        this.sizeX = 2*workHandler.width;
        this.sizeY = workHandler.height;
    },
    autoInit() {
        this.allConstructed = false;
        this.stocked = 0;
    },
    reinit: function() {
        let newList = new Array();
        for(let i=0; i<this.scaffoldList.length; i++) {
            let s1 = this.scaffoldList[i];
            let s2 = new Scaffold(s1.x, s1.y, s1.sizeX, s1.sizeY);
            s2.built = s1.built;
            s2.flag = undefined;
            let w = Math.floor(s2.sizeX/4);
            let x1 = Math.round(s2.sizeX/2 - w/2);
            s2.ladder = {x: x1+s2.x, w: w};
            newList.push(s2);
        }
        this.scaffoldList = newList;
    },
    add: function(x,y) {
        this.scaffoldList.push(new Scaffold(x, y, Math.floor(this.sizeX/UIHandler.zoomLevel), Math.floor(this.sizeY/UIHandler.zoomLevel)));
    },
    draw: function() {
        for(let i=0; i<this.scaffoldList.length; i++) {
            this.scaffoldList[i].draw();
        }
    },
    buildScaffold: function(scaffold) {
        scaffold.built = true;
        scaffold.draw();
    },
    nextScaffoldBuild: function() {
        let s = undefined;
        for(let i=0; i<this.scaffoldList.length; i++) {
            let s2 = this.scaffoldList[i];
            if(!s2.built && s2.flag === undefined) {
                if(s === undefined || s2.y > s.y)
                    s = s2;
            }
        }
        if(s !== undefined) {
            s.flag = "taken";
            return {x: s.x, y: s.y, name: "scaffold", scaffold: s};
        } else {
            this.allConstructed = true;
            return undefined;
        }
    },
    noNextScaffold: function() {
        return this.allConstructed;
    },
    locateLadder: function(x,y) {
        let i=0;
        let notFound = true;
        while(i < this.scaffoldList.length && notFound) {
            let s = this.scaffoldList[i];
            if(s.built && s.x <= x && s.x + s.sizeX >= x && s.y - s.sizeY <= y && s.y >= y) {
                notFound = false;
                return Math.round(s.ladder.x + s.ladder.w/2);
            } else { 
                i++;
            }
        }
        if(notFound) {
            return undefined;
        }
    },
    unbuildAll() {
        for(let i=0; i<this.scaffoldList.length; i++) {
            this.scaffoldList[i].built = false;
            this.scaffoldList[i].flag = undefined;
        }
    },
};

var prestigeHandler = {
    goldenBrick: 0,
    usableGoldenBrick: 0,
    unlocked: false,
    alreadyDrawn: false,
    listBricks: new Array(),
    tooltipDisplay: false,
    brickTooltip: undefined,
    confirmRestart: false,

    firstInit() {
        this.goldenBrick = 0;
        this.usableGoldenBrick = 0;
        this.unlocked = false;
        this.alreadyDrawn = false;
        this.listBricks = new Array();
        this.tooltipDisplay = false;
        this.brickTooltip = undefined;
        this.confirmRestart = false;
    },
    initialize() {
        this.confirmRestart = false;
        this.tooltipDisplay = false;
        this.brickTooltip = undefined;
        //this.listBricks.push(new PrestigeBrick(x,y,semi,title,tooltip,cost,effect,unlock, visible));
        let tooltip = "This brick has not been implemented yet, it has no effect whatsoever";
        let cost = 100000;
        let effect = "noEffect";
        this.listBricks.push(new PrestigeBrick(200,550,true,"1",tooltip,cost,effect, ["4"], true));
        this.listBricks.push(new PrestigeBrick(280,550,false,"2",tooltip,cost,effect, ["4","5"], true));
        this.listBricks.push(new PrestigeBrick(440,550,true,"3",tooltip,cost,effect, ["5"], true));
        this.listBricks.push(new PrestigeBrick(200,470,false,"4",tooltip,cost,effect, ["6","7"]));
        this.listBricks.push(new PrestigeBrick(360,470,false,"5",tooltip,cost,effect, ["7","8"]));

        this.listBricks.push(new PrestigeBrick(200,390,true,"6",tooltip,cost,effect, ["31"]));
        this.listBricks.push(new PrestigeBrick(280,390,false,"7",tooltip,cost,effect, ["31","32"]));
        this.listBricks.push(new PrestigeBrick(440,390,false,"8",tooltip,cost,effect, ["32","33"]));

        this.listBricks.push(new PrestigeBrick(760,550,true,"21",tooltip,cost,effect, ["24"], true));
        this.listBricks.push(new PrestigeBrick(840,550,false,"22",tooltip,cost,effect, ["24","25"], true));
        this.listBricks.push(new PrestigeBrick(1000,550,true,"23",tooltip,cost,effect, ["25"], true));
        this.listBricks.push(new PrestigeBrick(760,470,false,"24",tooltip,cost,effect, ["26","27"]));
        this.listBricks.push(new PrestigeBrick(920,470,false,"25",tooltip,cost,effect, ["27","28"]));

        this.listBricks.push(new PrestigeBrick(680,390,false,"26",tooltip,cost,effect, ["34","35"]));
        this.listBricks.push(new PrestigeBrick(840,390,false,"27",tooltip,cost,effect, ["35","36"]));
        this.listBricks.push(new PrestigeBrick(1000,390,true,"28",tooltip,cost,effect, ["36"]));

        this.listBricks.push(new PrestigeBrick(200,310,false,"31",tooltip,cost,effect, ["51","52"]));
        this.listBricks.push(new PrestigeBrick(360,310,false,"32",tooltip,cost,effect, ["52","53"]));
        this.listBricks.push(new PrestigeBrick(520,310,false,"33",tooltip,cost,effect, ["73"]));
        this.listBricks.push(new PrestigeBrick(680,310,true,"34",tooltip,cost,effect, []));
        this.listBricks.push(new PrestigeBrick(760,310,false,"35",tooltip,cost,effect, ["61","62"]));
        this.listBricks.push(new PrestigeBrick(920,310,false,"36",tooltip,cost,effect, ["62","63"]));

        this.listBricks.push(new PrestigeBrick(200,230,true,"51",tooltip,cost,effect, ["71"]));
        this.listBricks.push(new PrestigeBrick(280,230,false,"52",tooltip,cost,effect, ["72"]));
        this.listBricks.push(new PrestigeBrick(440,230,true,"53",tooltip,cost,effect, []));

        this.listBricks.push(new PrestigeBrick(760,230,true,"61",tooltip,cost,effect, []));
        this.listBricks.push(new PrestigeBrick(840,230,false,"62",tooltip,cost,effect, ["74"]));
        this.listBricks.push(new PrestigeBrick(1000,230,true,"63",tooltip,cost,effect, ["75"]));

        this.listBricks.push(new PrestigeBrick(200,150,true,"71",tooltip,cost,effect, []));
        this.listBricks.push(new PrestigeBrick(360,150,true,"72",tooltip,cost,effect, []));
        this.listBricks.push(new PrestigeBrick(600,230,true,"73",tooltip,cost,effect, []));
        this.listBricks.push(new PrestigeBrick(840,150,true,"74",tooltip,cost,effect, []));
        this.listBricks.push(new PrestigeBrick(1000,150,true,"75",tooltip,cost,effect, []));
    },
    reinit() {
        let newList = new Array();
        for(let i=0; i<this.listBricks.length; i++) {
            let oldB = this.listBricks[i];
            let newB = new PrestigeBrick(oldB.x, oldB.y, false, oldB.title, "", oldB.cost, []);
            newB.h = oldB.h;
            newB.w = oldB.w;
            if(oldB.unlock !== undefined)
                newB.unlock = JSON.parse(JSON.stringify(oldB.unlock));
            newB.tooltip = JSON.parse(JSON.stringify(oldB.tooltip));
            newB.built = oldB.built;
            newB.visible = oldB.visible;

            newList.push(newB);
        }
        this.listBricks = newList;
    },
    draw() {
        if(!this.alreadyDrawn) {
            UIHandler.drawButton(50,50,1200,600,true,"yellow2",ctxPrest);
            ctxPrest.lineWidth = 10;
            ctxPrest.strokeStyle = "red";
            ctxPrest.beginPath();
            ctxPrest.moveTo(1195,65);
            ctxPrest.lineTo(1235,105);
            ctxPrest.moveTo(1235,65);
            ctxPrest.lineTo(1195,105);
            ctxPrest.stroke();
            ctxPrest.fillStyle = "black";
            ctxPrest.font = "30px kenney_miniregular";
            ctxPrest.fillText("Golden Bricks: ",60,80);
            ctxPrest.fillText("After restart:", 60,110);
            for(let i=0; i<this.listBricks.length; i++) {
                this.listBricks[i].draw();
            }
            UIHandler.drawButton(540,500,200,70,true,"red",ctxPrest);
            ctxPrest.fillStyle = "black";
            if(this.confirmRestart) {
                ctxPrest.fillText("Are you sure?", 550, 545, 185);
            } else {
                ctxPrest.fillText("Restart", 580, 545);
            }
            this.alreadyDrawn = true;
        }
    },
    drawInfos() {
        ctxPrest.fillStyle = "rgba(245, 215, 110, 1)";
        ctxPrest.fillRect(280,60,300,70);
        ctxPrest.fillStyle = "black";
        ctxPrest.font = "30px kenney_miniregular";
        ctxPrest.fillText(this.usableGoldenBrick, 290, 80, 280);
        ctxPrest.fillText(this.goldenBrick, 290, 110, 280);
    },
    clearDraw() {
        ctxPrest.clearRect(0,0,1300,700);
        ctxPHover.clearRect(0,0,1300,700);
        this.alreadyDrawn = false;
        this.tooltipDisplay = false;
        this.brickTooltip = undefined;
    },
    clic(x,y) {
        if(x >= 50 && x <= 1250 && y >= 50 && y <= 650) {
            this.hover();
            if(this.tooltipDisplay) {
                let brick = this.brickTooltip;
                this.buy(brick);
                return true;
            }
            if(x >= 540 && x <= 740 && y >= 500 && y <= 570) {
                if(this.confirmRestart) {
                    this.prestige();
                    return true;
                } else {
                    this.confirmRestart = true;
                    this.alreadyDrawn = false;
                    return true;
                }
            }
            return true;
        }
        return false;
    },
    getPrestBrick(title) {
        let list = this.listBricks.filter(e => e.title == title);
        if(list.length > 0) {
            return list[0];
        }
        return undefined;
    },
    effect(effect) {
        //TODO
    },
    buy(brick) {
        if(!brick.built && this.usableGoldenBrick >= brick.cost) {
            this.usableGoldenBrick -= brick.cost;
            brick.built = true;
            for(let i=0; i<brick.unlock.length; i++) {
                let brick2 = this.getPrestBrick(brick.unlock[i]);
                if(brick2 !== undefined) {
                    brick2.visible = true;
                }
            }
            this.alreadyDrawn = false;
        }
    },
    hover() {
        let x = mouse.x;
        let y = mouse.y;
        let brick = this.whichBrickPointing(x,y);
        if(brick !== undefined && brick.visible) {
            if(this.tooltipDisplay && brick !== this.brickTooltip) {
                ctxPHover.clearRect(0,0,1300,700);
            }
            this.tooltipDisplay = true;
            this.brickTooltip = brick;
            brick.drawTooltip();
        } else if(this.tooltipDisplay) {
            ctxPHover.clearRect(0,0,1300,700);
            this.tooltipDisplay = false;
            this.brickTooltip = undefined;
        }
    },
    whichBrickPointing(x,y) {
        let i=0;
        let notFound = true;
        while(notFound && i < this.listBricks.length) {
            let brick = this.listBricks[i];
            if(x >= brick.x && x <= brick.x+brick.w && y >= brick.y && y <= brick.y + brick.h) {
                notFound = false;
            }
            i++
        }
        if(!notFound) {
            return this.listBricks[i-1];
        } else {
            return undefined;
        }
    },
    addGoldenBrick() {
        this.goldenBrick++;
    },
    prestige() {
        gameHandler.prestige = true;
        this.usableGoldenBrick += this.goldenBrick;
        this.goldenBrick = 0;
        this.clearDraw();
        this.confirmRestart = false;
        this.tooltipDisplay = false;
        this.brickTooltip = undefined;

        UIHandler.firstInit();
        brickHandler.firstInit();
        scaffoldHandler.firstInit();
        buildingHandler.initialize();

        soundHandler.musicOn = false;
        soundHandler.actualiseMusic();

        gameHandler.initialize();
    },
    applyEffects() {
        let list = this.listBricks.filter(e => e.built);
        for(let i=0; i<list.length; i++) {
            this.effect(list[i].effect);
        }
    }
}

var drawHandler = {
    stateDrawing: false,
    stateFilling: false,
    stateDrawFinished: false,
    trailList: new Array(),
    imageData: undefined,
    recursion: 0,
    flagStart: undefined,
    flagStop: undefined,

    initialize: function() {
        this.stateDrawing = false;
        this.stateFilling = false;
        this.recursion = 0;
        this.stateDrawFinished = false;
        let border = UIHandler.drawSurface;
        this.flagStart = {x: border.x + 50, y: border.y + border.h};
        this.flagStop = {x: border.x + border.w - 50, y: border.y + border.h};
    },
    start: function() {
        if(!this.stateDrawing && !this.stateDrawFinished) {
            gameHandler.state = "draw";
            this.stateDrawing = true;
            this.stateFilling = false;
            this.trailList = new Array();
            this.trailList.push(this.flagStart);
            brickHandler.initialize();
            scaffoldHandler.initialize();
        }
    },
    validate: function(doI) {
        ctxBrick.clearRect(0,0,canvasBrick.width,canvasBrick.height);
        ctxScaffold.clearRect(0,0,canvasScaffold.width,canvasScaffold.height);
        if(doI) {
            gameHandler.state = "construct";
        } else {
            gameHandler.state = "draw";
            this.stateDrawFinished = false;
            brickHandler.initialize();
            scaffoldHandler.initialize();
        }
    },
    stop: function() {
        if(this.stateDrawing) {
            this.stateDrawing = false;
            this.stateFilling = true;
            gameHandler.state = "pause";
            this.trailList.push(this.flagStop);
            this.trailList.push({x: this.trailList[0].x, y: this.trailList[0].y});
            context.clearRect(0, 0, canvas.width, canvas.height);
            this.draw();
            this.filling();
            if(this.stateDrawFinished)
                gameHandler.state = "validateConstruction";
            brickHandler.draw();
        }
    },
    filling: function() {
        this.imageData = context.getImageData(0,0, canvas.width, canvas.height);
        let surface = UIHandler.drawSurface;
        let sizeX = Math.floor(brickHandler.sizeX/UIHandler.zoomLevel);
        let sizeY = Math.floor(brickHandler.sizeY/UIHandler.zoomLevel);
        let altern = false;
        for(let j=surface.y + surface.h; j>=surface.y; j-=sizeY+2) {
            for(let i=surface.x; i<=surface.x + surface.w; i+=sizeX+2) {
                if(altern && i == surface.x) {
                    if(this.collide(i,j,Math.floor(sizeX/2), sizeY))
                        brickHandler.add(i,j,Math.floor(sizeX/2), sizeY);
                    i-=Math.floor(sizeX/2);
                } else if(this.collide(i,j,sizeX,sizeY))
                    brickHandler.add(i,j,sizeX,sizeY);
                else if(this.collide(i,j,Math.floor(sizeX/2),sizeY))
                    brickHandler.add(i,j,Math.floor(sizeX/2),sizeY);
                else if(this.collide(i+Math.floor(sizeX/2),j,Math.floor(sizeX/2),sizeY))
                    brickHandler.add(i+Math.floor(sizeX/2),j,Math.floor(sizeX/2),sizeY);
            }
            altern = !altern;
        }
        this.stateFilling = false;
        if(brickHandler.brickList.length <= 0) {
            this.start();
        } else {
            this.stateDrawFinished = true;
            //scaffolds
            sizeX = Math.floor(scaffoldHandler.sizeX/UIHandler.zoomLevel);
            sizeY = Math.floor(scaffoldHandler.sizeY/UIHandler.zoomLevel);
            let sizeBX = Math.floor(brickHandler.sizeX/UIHandler.zoomLevel);
            let sizeBY = Math.floor(brickHandler.sizeY/UIHandler.zoomLevel);
            for(let i=surface.x; i<surface.x + surface.w; i+=sizeX) {
                let j = surface.y;
                let brickX = i;
                let brickY = j;
                while((j <= surface.y + surface.h - sizeY) && (brickHandler.noBrickHere(brickX,brickY))) {
                    brickX += sizeBX/4;
                    if(brickX >= i + sizeX) {
                        brickX = i;
                        brickY += sizeBY/4;
                    }
                    if(brickY >= j + sizeY) {
                        j += sizeY;
                        brickX = i;
                        brickY = j;
                    }
                }
                let y = surface.y + surface.h - sizeY;
                while(y - j >= 0) {
                    scaffoldHandler.add(i,y);
                    y -= sizeY;
                }
            }
        }
    },
    collide: function(x,y,sizeX,sizeY) {
        let collision = 0;
        let collisionMin = sizeX * sizeY * 0.7;
        for(let i=x; i<x+sizeX; i++) {
            for(let j=y; j<y+sizeY; j++) {
                if(this.imageData.data[i*4 + j*4*canvas.width + 2] > 50) 
                    collision++;
            }
        }
        return collision > collisionMin;
    },
    draw: function() {  
        this.drawDrawingZone();      
        if(gameHandler.state === "draw") {
            ctxDZ.lineWidth = 3;
            ctxDZ.fillStyle = "rgba(150, 40, 27, 1)";
            ctxDZ.strokeStyle = "black";
            ctxDZ.fillRect(400, 20, 500, 50);
            ctxDZ.strokeRect(400, 20, 500, 50);
            ctxDZ.fillStyle = "rgba(30, 130, 76, 1)";
            ctxDZ.fillRect(405, 25, 490, 40);
            ctxDZ.strokeRect(405, 25, 490, 40);
            ctxDZ.font = "30px kenney_miniregular";
            ctxDZ.fillStyle = "black"
            ctxDZ.fillText("Draw the shape of the building", 418, 54);
        }
        context.beginPath();
        context.lineWidth = 5;
        if(this.stateDrawing)
            context.strokeStyle = 'rgba(150, 40, 27, 1)';
        else 
            context.strokeStyle = 'blue';
        for(let i=0; i < this.trailList.length; i++) {
            if(i == 0) 
                context.moveTo(this.trailList[i].x, this.trailList[i].y);
            else 
                context.lineTo(this.trailList[i].x, this.trailList[i].y);
        }
        if(this.stateDrawing)
            context.stroke();
        if(this.stateFilling) {
            context.stroke();
            context.fillStyle = 'blue';
            context.fill();
        }
    },
    drawDrawingZone: function() {
        this.clearDrawingZone();
        let surface = UIHandler.drawSurface;
        //cadre dessin si dessin en cours
        if(gameHandler.state == "draw") {
            ctxDZ.strokeStyle = 'red';
            ctxDZ.lineWidth = 2;
            ctxDZ.strokeRect(surface.x, surface.y, surface.w, surface.h);
            //flags départ et arrivée
            ctxDZ.lineWidth = 3;
            let fStart = this.flagStart;
            let fStop = this.flagStop;
            ctxDZ.fillStyle = 'grey';
            ctxDZ.fillRect(fStart.x - 2, fStart.y - 30, 5, 30);
            ctxDZ.fillRect(fStop.x - 2, fStop.y - 30, 5, 30);
            ctxDZ.fillStyle = 'green';
            ctxDZ.strokeStyle = 'grey';
            ctxDZ.lineWidth = 1;
            ctxDZ.beginPath();
            ctxDZ.moveTo(fStart.x - 2, fStart.y - 30);
            ctxDZ.lineTo(fStart.x - 22, fStart.y - 20);
            ctxDZ.lineTo(fStart.x - 2, fStart.y - 10);
            ctxDZ.stroke();
            ctxDZ.fill();
            ctxDZ.fillStyle = 'red';
            ctxDZ.beginPath();
            ctxDZ.moveTo(fStop.x + 2, fStop.y - 30);
            ctxDZ.lineTo(fStop.x + 22, fStop.y - 20);
            ctxDZ.lineTo(fStop.x + 2, fStop.y - 10);
            ctxDZ.stroke();
            ctxDZ.fill();
        }
    },
    clearDrawingZone: function() {
        ctxDZ.clearRect(0,0,canvasDrawingZone.width,canvasDrawingZone.height);
    },
    add: function() {
        if(this.stateDrawing) {
            let border = UIHandler.drawSurface;
            let x = mouse.x;
            let y = mouse.y;
            if(x < border.x) x = border.x;
            if(y < border.y) y = border.y;
            if(x > border.x + border.w) x = border.x + border.w;
            if(y > border.y + border.h) y = border.y + border.h;
            this.trailList.push({x: x, y: y});
        }
    }
};

var saveHandler = {
    file: undefined,
    countToSave: 500,
    importOpen: false,
    actualVersion: 1.2,

    initialize: function() {
        this.load();
    },
    load: function() {
        this.file = JSON.parse(localStorage.getItem('castle-builders'));
        if(this.verifySave()) {
            gameHandler.state = this.file.game.state;
            gameHandler.money = this.file.game.money;
            gameHandler.brickValue = this.file.game.value;
            
            workHandler.workerList = this.file.work.workers;
            workHandler.cooldown = this.file.work.cooldown;
            workHandler.speed = this.file.work.speed;
            workHandler.unemployed = this.file.work.unemployed;
            workHandler.width = this.file.work.width;
            workHandler.height = this.file.work.height;
            workHandler.startCost = this.file.work.startCost;
            workHandler.cost = this.file.work.cost;
            workHandler.costAugmentation = this.file.work.costAugmentation;
            
            if(this.file.UI.drawSurface !== undefined)
                Object.assign(UIHandler.drawSurface, this.file.UI.drawSurface);
            UIHandler.zoomLevel = this.file.UI.zoomLevel;
            UIHandler.x1 = this.file.UI.x1;
            UIHandler.x2 = this.file.UI.x2;
            UIHandler.y1 = this.file.UI.y1;
            UIHandler.y2 = this.file.UI.y2;
            if(this.file.UI.mine !== undefined)
                Object.assign(UIHandler.mine = this.file.UI.mine);
            if(this.file.UI.stockRock !== undefined)
                Object.assign(UIHandler.stockRock, this.file.UI.stockRock);
            if(this.file.UI.forest !== undefined)
                Object.assign(UIHandler.forest, this.file.UI.forest);
            if(this.file.UI.stockLumber !== undefined)
                Object.assign(UIHandler.stockLumber, this.file.UI.stockLumber);
            UIHandler.menuWorker = this.file.UI.menuWorker;
            UIHandler.menuUpgrade = this.file.UI.menuUpgrade;
            
            upgradeHandler.upgradeList = this.file.upgrade.upgradeList;
            if(this.file.upgrade.upgradeToCome !== undefined)
                Object.assign(upgradeHandler.upgradeToCome, this.file.upgrade.upgradeToCome);
            upgradeHandler.page = this.file.upgrade.page;
            upgradeHandler.newUpgrade = this.file.upgrade.newUpgrade;
            
            brickHandler.brickList = this.file.brick.brickList;
            brickHandler.stocked = this.file.brick.stocked;
            brickHandler.allConstructed = this.file.brick.allConstructed;
            brickHandler.buildingFinished = this.file.brick.buildingFinished;
            brickHandler.sizeX = this.file.brick.sizeX;
            brickHandler.sizeY = this.file.brick.sizeY;
            brickHandler.goldenChance = this.file.brick.goldenChance;
            
            scaffoldHandler.scaffoldList = this.file.scaffold.scaffoldList;
            scaffoldHandler.allConstructed = this.file.scaffold.allConstructed;
            scaffoldHandler.stocked = this.file.scaffold.stocked;
            scaffoldHandler.cost = this.file.scaffold.cost;
            scaffoldHandler.sizeX = this.file.scaffold.sizeX;
            scaffoldHandler.sizeY = this.file.scaffold.sizeY;
            
            drawHandler.stateDrawing = this.file.draw.stateDrawing;
            drawHandler.stateFilling = this.file.draw.stateFilling;
            drawHandler.stateDrawFinished = this.file.draw.stateDrawFinished;
            if(this.file.draw.trailList !== undefined)
                Object.assign(drawHandler.trailList, this.file.draw.trailList);
            drawHandler.recursion = this.file.draw.recursion;
            if(this.file.draw.flagStart !== undefined)
                Object.assign(drawHandler.flagStart, this.file.draw.flagStart);
            if(this.file.draw.flagStop !== undefined)
                Object.assign(drawHandler.flagStop, this.file.draw.flagStop);

            buildingHandler.current = this.file.building.current;
            if(this.file.building.data !== undefined)
                Object.assign(buildingHandler.buildList, this.file.building.data);
            if(this.file.building.src !== undefined)
                buildingHandler.reinit(this.file.building.src);

            soundHandler.musicOn = this.file.sound.musicOn;

            prestigeHandler.goldenBrick = this.file.prestige.goldenBrick;
            prestigeHandler.usableGoldenBrick = this.file.prestige.usableGoldenBrick;
            prestigeHandler.unlocked = this.file.prestige.unlocked;
            Object.assign(prestigeHandler.listBricks, this.file.prestige.listBricks);
            
            soundHandler.actualiseMusic();
            brickHandler.reinit();
            scaffoldHandler.reinit();
            upgradeHandler.reinit();
            workHandler.reinit();
            prestigeHandler.reinit();

            gameHandler.loading = true;
        }
        this.createFile();
    },
    save: function() {
        this.countToSave--;
        if(this.countToSave <= 0) {
            this.countToSave = 500;
            this.createFile();
            localStorage.setItem('castle-builders', JSON.stringify(this.file));
        }
    },
    createFile: function() {
        let buildingStrings = new Array();
        let ind = buildingHandler.order;
        for(let i=0; i<10; i++) {
            buildingStrings[ind[i]] = buildingHandler.listImage[ind[i]].src;
        }
        this.file = {
            "version": 1.2,
            "game": {
                "state": gameHandler.state,
                "money": gameHandler.money,
                "value": gameHandler.brickValue,
            },
            "work": {
                "workers": workHandler.workerList,
                "cooldown": workHandler.cooldown,
                "speed": workHandler.speed,
                "unemployed": workHandler.unemployed,
                "width": workHandler.width,
                "height": workHandler.height,
                "startCost": workHandler.startCost,
                "cost": workHandler.cost,
                "costAugmentation": workHandler.costAugmentation,
            },
            "UI": {
                "drawSurface": UIHandler.drawSurface,
                "zoomLevel": UIHandler.zoomLevel,
                "x1": UIHandler.x1,
                "x2": UIHandler.x2,
                "y1": UIHandler.y1,
                "y2": UIHandler.y2,
                "mine": UIHandler.mine,
                "stockRock": UIHandler.stockRock,
                "forest": UIHandler.forest,
                "stockLumber": UIHandler.stockLumber,
                "menuWorker": UIHandler.menuWorker,
                "menuUpgrade": UIHandler.menuUpgrade,
            },
            "upgrade": {
                "upgradeList": upgradeHandler.upgradeList,
                "upgradeToCome": upgradeHandler.upgradeToCome,
                "page": upgradeHandler.page,
                "newUpgrade": upgradeHandler.newUpgrade,
            },
            "brick": {
                "brickList": brickHandler.brickList,
                "stocked": brickHandler.stocked,
                "allConstructed": brickHandler.allConstructed,
                "buildingFinished": brickHandler.buildingFinished,
                "sizeX": brickHandler.sizeX,
                "sizeY": brickHandler.sizeY,
                "goldenChance": brickHandler.goldenChance,
            },
            "scaffold": {
                "scaffoldList": scaffoldHandler.scaffoldList,
                "allConstructed": scaffoldHandler.allConstructed,
                "stocked": scaffoldHandler.stocked,
                "cost": scaffoldHandler.cost,
                "sizeX": scaffoldHandler.sizeX,
                "sizeY": scaffoldHandler.sizeY,
            },
            "draw": {
                "stateDrawing": drawHandler.stateDrawing,
                "stateFilling": drawHandler.stateFilling,
                "stateDrawFinished": drawHandler.stateDrawFinished,
                "trailList": drawHandler.trailList,
                "recursion": drawHandler.recursion,
                "flagStart": drawHandler.flagStart,
                "flagStop": drawHandler.flagStop,
            },
            "building": {
                "src": buildingStrings,
                "data": buildingHandler.buildList,
                "current": buildingHandler.current,
            },
            "sound": {
                "musicOn": soundHandler.musicOn,
            },
            "prestige": {
                "goldenBrick": prestigeHandler.goldenBrick,
                "usableGoldenBrick": prestigeHandler.usableGoldenBrick,
                "unlocked": prestigeHandler.unlocked,
                "listBricks": prestigeHandler.listBricks,
            }
        }
    },
    erase: function() {
        localStorage.removeItem('castle-builders');
        UIHandler.firstInit();
        brickHandler.firstInit();
        scaffoldHandler.firstInit();
        buildingHandler.initialize();
        prestigeHandler.firstInit();

        soundHandler.musicOn = false;
        soundHandler.actualiseMusic();

        gameHandler.initialize();
    },
    verifySave: function() {
        if(this.file === undefined || this.file === null)
            return false;
        if(this.file.version === undefined || this.file.version !== this.actualVersion) {
            this.updateSave();
        }
        
        let saveOk = !(this.file === undefined || this.file === null || this.file.game === undefined || this.file.work === undefined  || this.file.UI === undefined || this.file.upgrade === undefined || this.file.brick === undefined || this.file.scaffold === undefined || this.file.draw === undefined || this.file.building === undefined || this.file.sound === undefined || this.file.prestige === undefined);
        return saveOk;
    },
    updateSave() {
        while(this.file.version !== this.actualVersion) {
            switch(this.file.version) {
                case undefined:
                    this.file.brick.goldenChance = 0.01;
                    this.file.prestige = {
                        "goldenBrick": prestigeHandler.goldenBrick,
                        "usableGoldenBrick": prestigeHandler.usableGoldenBrick,
                        "unlocked": prestigeHandler.unlocked,
                        "listBricks": prestigeHandler.listBricks,
                    };
                    this.file.version = 1.2;
                    break;
            }
        }
    },
    import: function() {
        if(this.importOpen) {
            let save = contentImportExport.value;
            try {
                this.file = JSON.parse(window.atob(save));
                if(this.verifySave()) {
                    localStorage.setItem('castle-builders', JSON.stringify(this.file));
                    this.countToSave = 500;
                    this.load();
                    openImportExport("Save successfully imported");
                }
            } catch(err) {
                contentImportExport.value = err;
            }
        } else {
            openImportExport("Import");
            this.importOpen = true;
        }
    },
    export: function() {
        this.createFile();
        openImportExport("Copied to clipboard");
        let str = window.btoa(JSON.stringify(this.file));
        contentImportExport.value = str;
        contentImportExport.select();
        document.execCommand("copy");
    },
};

var buildingHandler = {
    buildList: new Array(),
    listImage: new Array(),
    order: [1, 3, 0, 2, 6, 4, 5, 8, 9, 7],
    current: 0,

    initialize: function() {
        this.buildList = new Array();
        this.listImage = new Array();
        this.current = 0;
        for(let i=0; i<10; i++) {
            this.listImage[i] = new Image();
        }
        this.firstInit();
    },
    reinit: function(srcList) {
        for(let i=0; i<srcList.length; i++) {
            if(srcList[i] !== undefined) {
                this.listImage[i].src = srcList[i];
            }
        }
    },
    addNewBuilding: function() {
        //context.clearRect(0, 0, canvas.width, canvas.height);
        //brickHandler.draw();
        let sur = UIHandler.drawSurface;
        let max = UIHandler.zoomLevel*3+1;
        let indice = this.order[this.current];
        this.listImage[indice].src = canvasBrick.toDataURL("image/webp",0.1);
        console.log(indice);
        this.buildList[this.current] = {imgNo: indice, dx: sur.x, dy: sur.y, dw: sur.w, dh: sur.h, zoom: UIHandler.zoomLevel, loaded: false};
        this.current++;
        if(this.current >= max) this.current = 0;
    },
    draw: function() {
        let zoom = UIHandler.zoomLevel;
        let max = zoom*3 +1;
        for(let i=0; (i<this.buildList.length && i<max); i++) {
            let img = this.buildList[i];
            let y = 240 + (zoom-1)*80 - (zoom>2)*30;
            let x = 50 + (img.imgNo*1200/max) + 300/max;
            let sizeX = 600/max;
            let sizeY = img.dh*sizeX/img.dw;
            if(img.loaded) {
                ctxBG.drawImage(this.listImage[img.imgNo], img.dx, img.dy, img.dw, img.dh, x-sizeX*(img.zoom-1)/2, y-sizeY*(img.zoom-1), sizeX*img.zoom, sizeY*img.zoom);
            }
        }        
    },
    firstInit: function() {
        this.buildList = new Array();
        for(let i=0; i<10; i++) {
            this.listImage[i].src = "";
            this.listImage[i].onload = function() {
                imageBuildingLoaded(i);
            }
            this.buildList.push({imgNo: this.order[i], dx:0, dy:0, dw:0, dh:0, zoom: 0, loaded: false});
        }
    }
};

var soundHandler = {
    musicOn: false,
    sourceMusic: undefined,
    loaded: false,

    initialize: function() {
        this.musicOn = true;
        this.sourceMusic = new Audio("sound/music1genericbackgroundtrack.mp3");
        this.sourceMusic.loop = true;
        this.sourceMusic.load();
        const me = this;
        this.sourceMusic.addEventListener("load", function() {
            me.loaded = true;
            if(me.musicOn)
                this.sourceMusic.play();
        }, true);
    },
    changeHearabilityMusic: function() {
        this.musicOn = !this.musicOn;
        this.actualiseMusic();
    },
    actualiseMusic: function() {
        if(this.musicOn)
            this.sourceMusic.play();
        else
            this.sourceMusic.pause();
    }
};


/*------------------------------
Fix and bandaids
*/

/**
 * When an image is ready to be displayed: 
 * ----------------------------------
 * flag the image in the buildList:
 * *buildList[xx].loaded = true*
 * @param {int} i image indice 
 */
function imageBuildingLoaded(i) {
    for(let j=0; j<buildingHandler.order.length; j++) {
        let num = buildingHandler.order[j];
        if(num == i) {
            buildingHandler.buildList[j].loaded = true;
        }
    }
}

/**
 * calculate if all image are loaded before launching
 */
function oneMoreImgLoad() {
    imgLoading.current++;
}

function closeImportExport() {
    importExport.className = "hidden";
    UIHandler.menuImpExp = false;
    saveHandler.importOpen = false;
    contentImportExport.value = "Paste here your code, then click import";
}

function openImportExport(message) {
    importExport.className = "notHidden";
    infoImportExport.textContent = message;
}

function clicInfoImportExport() {
    saveHandler.import();
}

/*------------------------------
Animation et affichage
*/

function drawScreen() {
    //effacer
    if(gameHandler.state != "pause") {
        //clear all
        context.clearRect(0, 0, canvas.width, canvas.height);
        ctxFront.clearRect(0,0,canvasFront.width, canvasFront.height);
        ctxWorker.clearRect(0,0,canvasWorker.width,canvasWorker.height);

        /*
        //bottleneck in performance, in min brick size and max surface size, does take too much time to redraw each refresh
        //console.time("bricks");
        brickHandler.draw();
        //console.timeEnd("bricks");
        scaffoldHandler.draw();
        */

        UIHandler.drawSecondPaint();

        if(gameHandler.state == "draw")
            drawHandler.draw();

        if(gameHandler.state == "construct") {
            workHandler.draw();
            UIHandler.drawConstructInfos();
        }

        if(gameHandler.state == "finishConstruction")
            UIHandler.drawBuildingFinished();

        UIHandler.drawNumberInfos();
        UIHandler.drawMenu();
            
        if(upgradeHandler.mouseOn !== undefined)
            upgradeHandler.drawTooltip();
    }
}

function animate(timestamp) {
    if(lastTime == 0) lastTime = timestamp-1;
    if(timestamp - lastTime >= refresh || timestamp - lastTime == 0) {
        if(!gameHandler.amIWorking) {
            //console.time("core");
            gameHandler.core();
            //console.timeEnd("core");
            timer += timestamp - lastTime;
            lastTime = timestamp;
        }
    }
    window.requestAnimationFrame(animate);
}

/*------------------------------
Interaction utilisateur
*/

document.addEventListener('mousemove', e => {
    canvasPosition = canvas.getBoundingClientRect();
    mouse.x = Math.round(e.x - canvasPosition.left);
    mouse.y = Math.round(e.y - canvasPosition.top);
    if(gameHandler.state == "draw") {
        drawHandler.add();
    } else if(UIHandler.menuUpgrade) {
        upgradeHandler.mouseHover();
    } else if(UIHandler.menuPrestige) {
        prestigeHandler.hover();
    }
});

document.addEventListener('mousedown', e => {mouseDown()});
document.addEventListener('mouseup', e => {mouseUp()});

function mouseDown() {
    if(gameHandler.state == "draw")
        drawHandler.start();
    else 
        UIHandler.clic();
}

function mouseUp() {
    if(gameHandler.state == "draw")
        drawHandler.stop();
}

/*------------------------------
Démarrage de la page
*/

window.onload = function() {

    gameHandler.initialize();

    animate(performance.now());
}